#include "principal.h"

Principal::Principal(QWidget *parent)
    : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();


    vista->setLayout(layout);
}

Principal::~Principal()
{

}

void Principal::showMaximized()
{
    vista->showMaximized();
}

void Principal::SetModel(Modelo &m)
{
    miModelo = &m;
    LayoutCategorias();
    LayoutModos();
    LayoutFavoritos();
}

void Principal::hide()
{
    vista->hide();
}

void Principal::LayoutCategorias()
{
    Configuracion = new miBoton("Configuracion");
    connect(Configuracion, SIGNAL(clicked()), this, SLOT(configuracion()));
    connect(Configuracion, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    Sensores = new miBoton("Sensores");
    connect(Sensores, SIGNAL(clicked(bool)), this, SLOT(sensores()));
    connect(Sensores, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    Actuadores = new miBoton("Actuadores");
    connect(Actuadores, SIGNAL(clicked(bool)), this, SLOT(actuadores()));
    connect(Actuadores, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    Imagenes = new miBoton("Imagenes");
    connect(Imagenes, SIGNAL(clicked(bool)), this, SLOT(imagenes()));
    connect(Imagenes, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));

    QVBoxLayout *catLayout = new QVBoxLayout();
    catLayout->addWidget(Configuracion);
    catLayout->addWidget(Sensores);
    catLayout->addWidget(Actuadores);
    catLayout->addWidget(Imagenes);

    catLayout->setContentsMargins(0, 0, 30, 0);

    layout->addLayout(catLayout, 2, 0, 4, 1);
}

void Principal::LayoutModos()
{
    //Ojos
    QVBoxLayout *Ojos = new QVBoxLayout();
    ModoOjos = new miBoton("Ojos");
    connect(ModoOjos, SIGNAL(clicked(bool)), this, SLOT(cambiarmodo()));
    connect(ModoOjos, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    ModoOjos->setAutoExclusive(true);
    ModoOjos->setCheckable(true);
    etiquetaOjos = new QLabel();
    Ojos->addWidget(ModoOjos);
    Ojos->addWidget(etiquetaOjos);
    etiquetaOjos->setAlignment(Qt::AlignHCenter);

    //Tactil
    QVBoxLayout *tactil = new QVBoxLayout();
    ModoTactil = new miBoton("Tactil");
    connect(ModoTactil, SIGNAL(clicked(bool)), this, SLOT(cambiarmodo()));
    connect(ModoTactil, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    ModoTactil->setAutoExclusive(true);
    ModoTactil->setCheckable(true);
    etiquetaTactil = new QLabel();
    tactil->addWidget(ModoTactil);
    tactil->addWidget(etiquetaTactil);
    etiquetaTactil->setAlignment(Qt::AlignHCenter);

    //Voz
    QVBoxLayout *voz = new QVBoxLayout();
    ModoVoz = new miBoton("Voz");
    connect(ModoVoz, SIGNAL(clicked(bool)), this, SLOT(cambiarmodo()));
    connect(ModoVoz, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    ModoVoz->setAutoExclusive(true);
    ModoVoz->setCheckable(true);
    etiquetaVoz = new QLabel();
    voz->addWidget(ModoVoz);
    voz->addWidget(etiquetaVoz);
    etiquetaVoz->setAlignment(Qt::AlignHCenter);

    //Gestos
    QVBoxLayout *Gestos = new QVBoxLayout();
    ModoGestos = new miBoton("Gestos");
    connect(ModoGestos, SIGNAL(clicked(bool)), this, SLOT(cambiarmodo()));
    connect(ModoGestos, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
    ModoGestos->setAutoExclusive(true);
    ModoGestos->setCheckable(true);
    etiquetaGestos = new QLabel("Dispositivo: Cabecero");
    Gestos->addWidget(ModoGestos);
    Gestos->addWidget(etiquetaGestos);
    etiquetaGestos->setAlignment(Qt::AlignHCenter);

    QGroupBox *Modos = new QGroupBox("Modos de Funcionamiento");
    QHBoxLayout *LayoutModos = new QHBoxLayout();
   // Modos->setStyleSheet("background-color: rgb(130,93,24)");
    //LayoutModos->addWidget(ModoOjos);
    LayoutModos->addLayout(Ojos);
    LayoutModos->addLayout(tactil);
    LayoutModos->addLayout(voz);
    LayoutModos->addLayout(Gestos);

    LayoutModos->setSpacing(50);
    Modos->setLayout(LayoutModos);

    int x1 = Modos->x();
    int y1 = Modos->y() + Modos->height()/2;
    int x2 = Modos->x() + Modos->width();
    int y2 = Modos->y() + Modos->height()/2;
    int y = Modos->y();
 //   QPainter(Modos).drawLine(QPoint(x1,y1),QPoint(x2,y2));
    QPainter(Modos).drawLine(QPoint(0,Modos->height()/2), QPoint(Modos->width(),Modos->height()/2));
    QPainter painter(this);
    painter.setBrush(QBrush(Qt::green));
    painter.drawLine(x1, y1, x2, y2);


    layout->addWidget(Modos, 6, 0, 1, 3);
}

void Principal::LayoutFavoritos()
{
    Favs = new QGroupBox("Favoritos");
    layoutfavoritos = new QVBoxLayout();
    //Favs->setStyleSheet("background-color: rgb(130,93,24)");

    layoutfavoritos->setSpacing(30);
    Favs->setLayout(layoutfavoritos);
    layout->addWidget(Favs, 2, 1, 4, 2);
}

void Principal::DibujarBotones(miBoton *bot)
{
    layoutfavoritos->addWidget(bot);
    bot->setVisible(true);
}

void Principal::EliminarBotones(miBoton *bot)
{
    layoutfavoritos->removeWidget(bot);
    bot->setVisible(false);
}

void Principal::DibujarDimmer(QWidget *widget)
{
    layoutfavoritos->addWidget(widget);
    widget->setVisible(true);
}

void Principal::EliminarDimmer(QWidget *widget)
{
    layoutfavoritos->removeWidget(widget);
    widget->setVisible(false);
}


void Principal::styleSheet(QString str)
{
    vista->setStyleSheet(str);
}

void Principal::cambioModo(QString str)
{
    QString aux = "Modo Activo: " + str;
    etiquetaOjos->setText(aux);
}

void Principal::clickarBoton(QString str)
{
    if (str == "Ojos")
        ModoOjos->setChecked(true);
    else if(str == "Tactil")
        ModoTactil->setChecked(true);
    else if(str == "Voz")
        ModoVoz->setChecked(true);
    else if (str == "Gestos")
        ModoGestos->setChecked(true);
    else
        return;
}

void Principal::configuracion()
{
    miModelo->CambioVentana("Configuracion");
}

void Principal::sensores()
{
    miModelo->CambioVentana("Sensores");
}

void Principal::actuadores()
{
    miModelo->CambioVentana("Actuadores");
}

void Principal::imagenes()
{
    miModelo->CambioVentana("Imagenes");
}

void Principal::cambiarmodo()
{
    if (ModoOjos->isChecked())
        miModelo->CambiarModoActivo("Ojos");
    else if (ModoTactil->isChecked())
        miModelo->CambiarModoActivo("Tactil");
    else if (ModoVoz->isChecked())
        miModelo->CambiarModoActivo("Voz");
    else if (ModoGestos->isChecked())
        miModelo->CambiarModoActivo("Gestos");
}

