#include "principal.h"
#include "modelo.h"
#include "configuracion.h"
#include "conexion.h"
#include "sensores.h"
#include "cuadrotexto.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Configuracion ventana_configuracion;
    Principal ventana_principal;
    Conexion conexion;
    Sensores sens;
    Actuadores act;
    CuadroTexto cuad;

    Modelo m(ventana_principal, ventana_configuracion, conexion, sens, act, cuad);
    ventana_configuracion.SetModel(m);
    ventana_principal.SetModel(m);
    conexion.SetModel(m);
    sens.setModel(m);
    act.setModel(m);

   // m.principal->showMaximized();

    return a.exec();
}
