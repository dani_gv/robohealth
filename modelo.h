#ifndef MODELO_H
#define MODELO_H

#include <QObject>
#include <QVector>
#include <vector>
#include <string>
#include <QString>
#include <QUrl>
#include <QElapsedTimer>
#include <QWidget>
#include <QCursor>
#include <QTime>
#include <QTimer>
#include <QDebug>
//#include <windows.h>
#include <QDebug>

#include "bbdd.h"
#include "eyex.h"
#include "configuracion.h"
#include "principal.h"
#include "conexion.h"
#include "sensores.h"
#include "actuadores.h"
#include "cuadrotexto.h"
#include "veracontroller.h"
#include "miboton.h"
#include "mislider2.h"


class Configuracion;
class Principal;
class Conexion;
class Sensores;
class Actuadores;
class CuadroTexto;

using namespace std;

#define TIME_DECO (10000) // Time before automatic disconnect, if the controller doesn't answer


class Modelo : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Modelo Constructor del modelo que recibe como parámetros un objeto de cada ventana para asignarlo a sus atributos.
     * Además, realiza las conexiones del EyeTracker con él, establece la pantalla inicial a la conexión y los modos activos de
     * manera inicial.
     * @param p
     * @param conf
     * @param conex
     * @param sens
     * @param act
     * @param cuad
     * @param parent
     */
    explicit Modelo(Principal &p, Configuracion &conf, Conexion &conex, Sensores &sens, Actuadores &act, CuadroTexto &cuad, QObject *parent = 0);



    //BBDD
        //Usuarios
    /**
     * @brief Las siguientes son funciones concernientes al EyeTracker para, en función del botón pulsado desde la ventana
     * configuración, realizar la acción pertinente respecto a los usuarios del EyeTracker.
     */
    void CrearUsuario();
    void EliminarUsuario(QString str);
    void RecalibrarUsuario();
    void ComprobarCalibracion();
    void CambiarUsuario(QString str);

    /**
     * @brief CambiarModoActivo establece el modo activo en la BBDD y, en caso de que sea el modo "Ojos", indica al EyeTracker
     * que empiece a realizar las interacciones pertinentes.
     * @param str es el nombre del modo activo.
     */
    void CambiarModoActivo(QString str);
    /**
     * @brief getModoActivo
     * @return devuelve el nombre del modo activo.
     */
    QString getModoActivo();


        //Favoritos
    /**
     * @brief Los siguientes métodos ejecutan cambios en los favoritos que tiene guardados la BBDD, además de obtener
     * todos los favoritos para poder dibujarlos en la pantalla principal.
     */
    void AnyadirFavorito(QString str);
    void EliminarFavorito(QString str);
    QVector<QString>GetFavoritos();


        //Devices
    /**
     * @brief GetDevices Obtiene los dispositivos guardados en la BBDD
     * @return la lista de nombres de los dispositivos.
     */
    QVector<QString>GetDevices();

    //Cambio de Ventana activa
    /**
     * @brief CambioVentana Realiza el cambio de ventana que se está mostrando, escondiendo las demás.
     * Además, introduce los botones correspondientes en cada una de las ventanas en función de qué
     * ventana tenga que mostrar.
     * @param str nombre de la ventana a mostrar
     */
    void CambioVentana(QString str);


    //Tobii
    /**
     * @brief Las siguientes funciones establecen el nuevo tiempo para calcular el cierre, el guiño o la fijación.
     * En caso de la fijación, sólo lo guarda en la BBDD, mientras que en los otros dos, lo guarda en la BBDD y se lo
     * comunica al EyeTracker.
     */
    void TiempoFijacion(int time);
    void TiempoGuino(int time);
    void TiempoCierre(int time);

    /**
     * @brief setModoClick Avisa a la BBDD del modo que está activo para hacer click (Fijación o Guiño).
     * @param mode
     */
    void setModoClick(QString mode);
    /*
     * Esta función sirve para mantener el ratón fijo sobre un botón
     *
    */
    /**
     * @brief ColocarMouse se emplea para mantener el cursor fijo sobre el botón
     * al que el usuario esté mirando. Además, coloca el cursor en el centro del botón
     * mirado.
     * @param x: coordenada recibida por el EyeTracker de dónde está mirando el usuario en X.
     * @param y: coordenada recibida por el EyeTracker de dónde está mirando el usuario en Y.
     */
    void ColocarMouse(float x, float y);


    //Principal
    /**
     * @brief connection Establece la conexión con el controlador.
     * @param str
     */
    void connection(QString str);

    //Vera Controller
    /**
     * @brief CreateDevices recoge los dispositivos que posee Vera al comenzar el programa para poder usarlos
     * posteriormente. Además, también realiza las conexiones entre esos botones y los slots correspondientes.
     */
    void CreateDevices();
    /**
     * @brief clear elimina todos los dispositivos guardados y sus conexiones.
     */
    void clear();
    /**
     * @brief crearCuadro se emplea para crear un cuadro de texto con la información metida como atributo.
     * @param inf
     */
    void crearCuadro(QString inf);
    /**
     * @brief habilitarVentanas es una función creada para habilitar todas las ventanas tras ver el problema
     * ocurrido con un cuadro de texto.
     */
 //   void habilitarVentanas();
private:

    BBDD *bbdd;
    EyeX *eye;
    Principal *principal;
    Configuracion *configuracion;
    Conexion *conexion;
    veraController *controller;
    Sensores *sensor;
    Actuadores *actuador;
    CuadroTexto *cuadro;

    QString stylesheet;

    /**
     * @brief Click simula un click donde se encuentre el cursor.
     */
    void Click();
    /**
     * @brief ComprobacionTiempos comprueba si existe una fijación por parte del usuario o no
     * ha transcurrido un tiempo suficiente.
     * @param tInicial tiempo donde empieza la fijación
     * @param tFinal tiempo actual
     * @param tFijacion tiempo mínimo de la fijación.
     * @return true en caso de que haya habido fijación, false en caso contrario.
     */
    bool ComprobacionTiempos(QTime tInicial, QTime tFinal, int tFijacion);
    /**
     * @brief establecerStyleSheet busca cuál es la pantalla activa, mandándole a ésta la hoja de estilos
     * para que configure su estilo.
     */
    void establecerStyleSheet();
    /**
     * @brief habilitacion se usa para poder deshabilitar una pantalla en caso de fallo.
     */
    void habilitacion(bool);

    QElapsedTimer *automaticDecoTimer;
    QString nombreBoton;

  //  int lastDimmerTrigerred;
   // QTimer *dimmerTimer;

    //Para probar, aunque luego irá en el modelo!
       //void ColocacionRaton(miBoton *); //Esta como SLOT
       // void ColocarMouse(float x, float y); //Esta como Funcion publica
       QTime tFinal;
       QTime tInicial;
       bool mouseHabilitation;
       float xCentro;
       float yCentro;
       float x1;
       float x2;
       float y1;
       float y2;
       const int margenPixelBotones = 50;

       bool Clickado;

       QVector<miBoton *>luces;
       QSignalMapper *lucesMapper;

       QVector<miBoton *>sirenas;
       QSignalMapper *sirenasMapper;

       QVector<miBoton *>sensores;

       QVector<miSlider2 *>sliders;

       QVector<miSlider2 *>blinds;

       miBoton *alerta;
       bool ojoscerrados;
       miBoton *focus;
       QTime prueba;
       const int tamanoIcono = 70;


signals:

public slots:
    //Tobii
     /**
     * @brief ActualizarUsuarios recibe la lista de usuarios del EyeTracker y se la manda a la pantalla
     * configuración para que la muestre en la lista y a la BBDD para que lo guarde.
     */
    void ActualizarUsuarios(vector<QString>);
    /**
     * @brief setUsuarioActual realiza la misma operación que ActualizarUsuarios pero sólo con el usuario actual.
     */
    void setUsuarioActual(QString);
    /**
     * @brief Posicionar recibe las coordenadas del EyeTracker donde el usuario está mirando.
     * @param x
     * @param y
     */
    void Posicionar(float x, float y);
    /**
     * @brief Guino_ojo se ejecuta cuando el EyeTracker ha detectado un guiño, colocando el cursor en ese punto
     * y simulando un click.
     */
    void Guino_ojo();
    /**
     * @brief Cierre se ejecuta cuando el EyeTracker detecta un cierre de ojos y se emplea para cambiar el
     * modo de hacer click mediante la vista.
     */
    void Cierre();

    /**
     * @brief Colocacion_Raton se ejecuta cuando el cursor entra dentro de un botón. En ese caso, calcula cuáles
     * son las coordenadas del centro del botón guardándolas para fijar el cursor en ese punto en caso de que
     * el usuario mire a otra parte del botón.
     * @param b es el botón sobre el que el usuario está mirando.
     */
    void Colocacion_Raton(miBoton *b);

    //Vera Controller
    /**
     * @brief connectionReply se ejecuta cuando la conexión con el controlador es válida y se emplea para
     * crear todos los dispositivos obtenidos de Vera. Además, establece la pantalla principal como activa.
     * @param ok
     */
    void connectionReply(bool ok);
    /**
     * @brief UpdateDevices es un slot llamado cuando se ha producido algún cambio en el estado de alguno de los
     * dispositivos que posee el controlador. Cuando se llama, comprueba que todo sea válido y actualiza el
     * estado de los dispositivos con esta información.
     * @param ok
     * @param d especifica el tipo del dispositivo modificado.
     * @param id especifica el identificador del dispositivo modificado.
     */
    void UpdateDevices(bool ok, Vera::Type d, int id);
    /**
     * @brief sensorSignalReceived se ejecuta cuando un sensor recibe una señal y, en función de lo que reciba,
     * lo actualiza para mostrar si está encendido o disparado.
     * @param on se refiere a si el sensor está disparado.
     * @param id se refiere al identificador del sensor, para poder cambiarle el estado.
     */
    void sensorSignalReceived(bool on, int id);

    //Dimmers
    /**
     * @brief cambioValorDimmer se ejecuta cuando un objeto de la clase miSlider2 ha cambiado de valor.
     * En este método se busca qué dimmer ha cambiado el valor y se ordena al controlador que cambie el valor
     * para que se pueda observar.
     * @param dimmer
     */
    void cambioValorDimmer(miSlider2 *dimmer);
    void cambioValorBlind(miSlider2* blind);

    void ojosAbiertos();

private slots:
    /**
     * @brief switchSiren cambia el estado de la sirena en función de si el botón está pulsado o no.
     * @param num
     */
    void switchSiren(int num);

};

#endif // MODELO_H
