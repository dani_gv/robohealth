#ifndef EYEX_H
#define EYEX_H

#include <QObject>
#include <QTime>
#include <vector>
#include <QVector>
#include <windows.h>
#include <QString>

#include "Componentes/includes/eyex/EyeX.h"

#pragma (lib, "Tobii.EyeX.Client.lib")

using namespace std;

class EyeX : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief EyeX: Inicialización de atributos propios y conexión con los métodos propios del EyeTracker
     * para su funcionamiento. Se realiza la conexión directamente con funciones independientes que actúan
     * como callbacks puesto que los métodos de una clase no pueden actuar como tal.
     * @param parent
     */
    explicit EyeX(QObject *parent = 0);
    ~EyeX();

    /**
     * @brief Las siguientes funciones son necesarias para el funcionamiento del EyeTracker y son propias
     * del dispositivo. Se llaman a través de las funciones independientes que actúan como callbacks.
     */
    void OnSnapshotCommitted(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param);
    void HandleEvent(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam);
    void OnEngineConnectionStateChanged(TX_CONNECTIONSTATE connectionState, TX_USERPARAM userParam);
    bool InitializeGlobalInteractorSnapshot(TX_CONTEXTHANDLE hContext);
    void OnEngineStateChanged(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam);

    /**
     * @brief CambiarUsuario: Método llamado por el modelo para cambiar el usuario activado en el dispositivo.
     * @param name: Nombre del usuario a cambiar.
     */
    void CambiarUsuario(QString name);
    /**
     * @brief EliminarUsuario: Método llamado por el modelo para eliminar un usuario de la lista que el dispositivo
     * tiene guardada.
     * @param name: Nombre del perfil a eliminar.
     */
    void EliminarUsuario(QString name);
    /**
     * @brief CrearUsuario: Método llamado por el modelo para indicarle al dispositivo que realice el proceso de añadir
     * nuevo usuario y, además, actualice la lista de usuarios disponibles.
     */
    void CrearUsuario();
    /**
     * @brief Recalibrar: Método llamado por el modelo para indicarle al dispositivo que realice el proceso de recalibrar
     * el dispositivo para el usuario activado en ese momento.
     */
    void Recalibrar();
    /**
     * @brief ComprobarCalibracion: Realiza la misma tarea que el anterior pero sólo comprueba la calibración, no recalibra.
     */
    void ComprobarCalibracion();
    /**
     * @brief IniciarPrograma se emplea para que el modelo avise al programa de
     * cuándo se está utilizando el modo "Ojos". De esta manera, se empezaría
     * a iterar para comprobar si ha habido algún guiño o cierre de ojos.
     */
    void IniciarPrograma();

    /*
     * Cambiar el tiempo de guiño establecido en configuracion
    */
    /**
     * @brief CambiarTiempoGuino cambia el tiempo establecido para considerar un guiño como click.
     * @param t
     */
    void CambiarTiempoGuino(int t);
    /**
     * @brief CambiarTiempoCierre cambia el tiempo establecido para considerar un cierre de ojos como
     * acción especial y no como pestañeo.
     * @param t
     */
    void CambiarTiempoCierre(int t);

private:

    //Funcioamiento de Tobii
    TX_CONTEXTHANDLE hContext;
    bool success;
    /**
     * @brief OnStateReceived: Método llamado cuando se produce algún cambio en el EyeTracker. Este método es el
     * encargado de llamar a los métodos donde se ejecutan todas las operaciones de cálculo de tiempos o de cómo
     * se encuentran los ojos.
     * @param hStateBag
     */
    void OnStateReceived(TX_HANDLE hStateBag);
    /**
     * @brief GetStateValueAsArrayOfStrings
     * @param hStateBag
     * @param valuePath
     * @param arrayOfStrings
     * @return
     */
    TX_BOOL GetStateValueAsArrayOfStrings(TX_CONSTHANDLE hStateBag, TX_CONSTSTRING valuePath, std::vector<std::string> *arrayOfStrings);

    /**
     * @brief ComprobacionClick: En esta función se entra cada vez que haya un cambio en la posición de los ojos
     * del usuario, o de su mirada hacia la pantalla. En ella, se evalúa qué hay que hacer
     * (Si emitir señales de click o de cierre de ojos).
     * @param hEyePositionDataBehavior
     */
    void ComprobacionClick(TX_HANDLE hEyePositionDataBehavior);
    /**
     * @brief ComprobacionTiempos: Método que evalúa si la diferencia de los tiempos introducidos
     * es mayor que el tiempo límite establecido.
     * @param t1 debe ser mayor que t2
     * @param t2
     * @param tiempoLimite
     * @return True si la diferencia de tiempos es mayor al tiempo límite.
     */
    bool ComprobacionTiempos(QTime t1, QTime t2, int tiempoLimite);
    /**
     * @brief ComprobacionOjos Método que estudia las coordenadas (de manera tridimensional) que tienen
     * ambos ojos. En función de lo que resulte de estas coordenadas, determina si se tienen los ojos
     * cerrados, abiertos o algún ojo guiñado.
     * @param RightEyeX
     * @param RightEyeY
     * @param RightEyeZ
     * @param LeftEyeX
     * @param LeftEyeY
     * @param LeftEyeZ
     */
    void ComprobacionOjos(double RightEyeX, double RightEyeY, double RightEyeZ, double LeftEyeX, double LeftEyeY, double LeftEyeZ);
    /*
     * La funcion Position recoge continuamente los valores donde el usuario está mirando en la pantalla.
    */
    /**
     * @brief Position recoge continuamente los valores de las coordenadas donde el usuario está mirando en la pantalla.
     * @param hFixationDataBehavior
     */
    void Position(TX_HANDLE hFixationDataBehavior);
    /*
     * La funcion filtro recibe los datos de la posición donde está mirando el usuario.
     * Esta es invocada desde la función Position y se usa para suavizar los datos recibidos,
     * evitando los cambios bruscos que produce la precisión de Tobii.
    */
    /**
     * @brief Filtro recibe los datos de la posición donde está mirando el usuario.
     * Esta es invocada desde la función Position y se usa para suavizar los datos recibidos,
     * evitando los cambios bruscos que produce la precisión de Tobii. Una vez los suaviza, emite
     * la posición de dónde tiene el modelo que colocar el cursor.
     * @param x_i: coordenada en x recibida, que será introducida en el vector de coordenadas X.
     * @param y_i: coordenada en y recibida, que será introducida en el vector de coordenadas Y.
     */
    void Filtro(float x_i, float y_i);

    /*
     * Tiempos utilizados para saber cuándo interacciona el usuario mediante sus ojos. Gracias a ellos
     * y sus comparaciones, se sabrá si se ha guiñado algún ojo o se han cerrado durante cierto tiempo.
    */
    QTime OjosAbiertos;
    QTime OjosInteraccion;

    QString UsuarioActual;
    vector<string>Usuarios;

    /*
     * Las siguientes variables se corresponden a:
     * Tiempo a superar necesariamente si se quieren cerrar los ojos para salir del programa.
     * Tiempo a superar si se quiere hacer click guiñando algun ojo.
    */
    int TiempoCierre;
    int TiempoGuino;

    /*
     * Ambos vectores van recogiendo datos con las coordenadas obtenidas del tobii.
     * Cuando se recogen la cantidad correspondiente al tamaño de ventana, se calculará la media
     * de estos vectores, colocando el ratón en esos puntos concretos.
     * Además, con la variable llamada MargenError, controlamos los valores que pueden entrar o no en ambos vectores
     * y cuándo hemos cambiado de punto de visión.
    */
    QVector<float>x;
    QVector<float>y;
    const int TamVentana = 30;


    /*
     * Variable utilizado para realizar la secuencia de guiño/cierre adecuadamente
    */
    bool Habilitacion;
    /*
     * Variable utilizada para hacer saber al programa que ya tiene los ojos cerrados
     * por lo que tiene que dejar de realizar las iteraciones pertinentes
    */
//    bool OjosCerrados;
    /*
     * Variable que está a "true" cuando se está en el modo Ojos y está a "false" si no se está
    */
    bool Inicializacion;

    /*
     * Este enum representa el estado actual de los ojos del usuario, que pueden ser 4:
     * Ojo derecho guiñado
     * Ojo izquierdo guiñado
     * Ambos ojos cerrados
     * Ambos ojos abiertos
    */
    enum Ojos
    {
        OjoDerechoCerrado,
        OjoIzquierdoCerrado,
        AmbosOjosCerrados,
        AmbosOjosAbiertos
    }EstadoActual;

signals:
    /*
     * Se emiten cuando hay un guiño derecho o izquierdo.
     * Se han separado por si en el futuro se quieren separar
     * para añadir alguna funcionalidad más al código
    */
    /**
     * @brief Las siguientes señales se emiten cuando hay un guiño derecho o izquierdo.
     * Se han separado por si en el futuro se quieren separar
     * para añadir alguna funcionalidad más al código en función del guiño.
     */
    void GuinoDerecho();
    void GuinoIzquierdo();
    /*
     * Posicionar se emite continuamente una vez ha empezado el programa
     * para colocar el ratón en las coordenadas correspondientes a la
     * mirada del usuario
    */
    /**
     * @brief Posicionar se emite continuamente una vez se ha empezado el programa
     * para colocar el cursor en las coordenadas correspondientes a la mirada
     * del usuario.
     * @param x es la coordenada en X de la pantalla después del suavizado.
     * @param y es la coordenada en Y de la pantalla después del suavizado.
     */
    void Posicionar(float x, float y);
    /*
     * Cierre ojos se emite cuando se mantienen los ojos cerrados durante un tiempo
     * suficiente para superar el tiempo de cierre de ojos correspondiente a un
     * pestañeo habitual.
    */
    /**
     * @brief cierreOjos se emite cuando se mantienen los ojos cerrados durante un tiempo
     * suficiente para superar el tiempo de cierre de ojos correspondiente a un
     * pestañeo habitual.
     */
    void cierreOjos();

    /**
     * @brief SenyalUsuarioActual se emite cuando hay un cambio en el usuario activo.
     * @param user es el usuario actual que se encuentra activo.
     */
    void SenyalUsuarioActual(QString user);
    /**
     * @brief ListaUsuarios se emite cuando hay un cambio en la lista general de usuarios que tiene el tobii.
     * @param Usuarios es la nueva lista de usuarios que posee el EyeTracker.
     */
    void ListaUsuarios(vector<QString>Usuarios);    

    void ojosAbiertos();
};

#endif // EYEX_H
