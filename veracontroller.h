#ifndef VERACONTROLLER_H
#define VERACONTROLLER_H

#include <QtNetwork>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTextStream>
#include <QTimer>

#include <vector>
#include <string>

#include "request.h"

#include "VeraDevices.h"

/**
 * Use setTimer to automatically call refresh. The state of the network is
 * updated when a request is done, without waiting for the system answer.
 * Therefore it may be corrected on the next call to refresh.
 * The first implementation didn't save the state, it was done on the next
 * refresh.
 * Both methods have their pros and cons, linked to the latency of the Vera, the
 * number of corrections needed before the state of the network is correct...
 * A good idea is to use the same solution for the display as the one
 * implemented here.
 * (the 2 solutions are :- considering that the request works, and correct it on
 *                         refresh if it didn't
 *                       - doing the request without changing anything, and
 *                         change on refresh if the request worked)
 *
 * The pros of the chosen solution are: - corrections only if the request did
 *                                        NOT work
 *                                      - no latency for display, without having
 *                                        to check for the Vera answer
 */

class veraController : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief The default constructor doesn't connect to the controller
     * Use the slot connection to do so
     */
    veraController();
    ~veraController();

    /**
     * @brief getNumberOf
     * @return the number of devices of the specified type
     */
    int getNumberOf(Vera::Type);
    /**
     * @brief getNameOf
     * @return the name of the of the nth device of the specified type
     * 0 <= n < getNumberOf(type) n is not the id of the device in the network
     */
    QString getNameOf(Vera::Type, int);
    /**
     * @brief getStateOfLamp
     * @return the state of the nth binary lamp (on/off)
     */
    bool getStateOfLamp(int);
    /**
     * @brief switchLampUrl: in most cases, consider using switchLamp(int)
     * @param num: number of the binary light
     * @return the URL to request to change the state of the specified light
     */
    QUrl switchLampUrl(const int num);
    /**
     * @brief changeDimmerValueUrl: consider using changeValueDimmer(int, int);
     * @param num: number of the dimmer
     * @param val: target value
     * @return the URL to request to change the value
     */
    QUrl changeDimmerValueUrl(const int num, const int val);
    /**
     * @brief changeSirenValueUrl: same as changeDimmerValueUrl, since a siren
     * can be seen as a dimmer, with redondant levels
     * Consider using changeStateSiren(int, bool, bool)
     * @param num: number of the siren
     * @param val: target value
     * @return the URL to request to change the value
     */
    QUrl changeSirenValueUrl(const int num, const int val);
    /**
     * @brief getStateOfDimmer
     * @return the state of the nth dimmer (0 <= state <= 100)
     */
    int getStateOfDimmer(int);
    /**
     * @brief getStateOfSensor
     * @return the state of the nth sensor
     */
    bool getStateOfSensor(int);
    /**
     * @brief getStateOfSensor
     * @return true if the sensor is currently tripped
     */
    bool getSensorTripped(int);
    /**
     * @brief getStateOfSiren
     * @return the state of the siren (light+sound) or the lamp if lamp=true
     */
    bool getStateOfSiren(int id, bool lamp=false);
    /**
     * @brief connection: try to connect to the controller at the specified
     * URL/ip
     * Emits connectionStatus(true) if the answer is valid,
     * connectionStatus(false) otherwise
     */
    void connection(QString);
    /**
     * @brief setTimer: start or stop the automatic refresh
     * @param on: true to launch the timer, false to stop it
     * @param msec: interval of refresh (not used if on=false)
     */
    void setTimer(bool on, int msec=5000);


    int getStateOfBlind(int id);
    QUrl changeBlindValueUrl(const int num, const int val);
    /* - All these methods are meant to be called after the connection to the
     * controller has been established, by calling connection(QString) and
     * receiving the signal connectionStatus(true)
     * - There is no problem in calling these methods before connecting to the
     * controller but there is no point
     * - Setting the timer on is not a valid way to connect to the controller
     * (see the brief of refresh())
     * - The int parameter which represents the "number of the device" in all
     * these methods, except getNumberOf and setTimer, can take all the values
     * between 0 and the result of getNumberOf - 1 included. This is NOT the id
     * of the device in the network. This is also true for the slots switchLamp,
     * changeValueDimmer and changeStateSiren.
     */


    //  void switchLamp(QString str);
public slots:

    /**
     * @brief switchLamp: change the state of the nth binary light
     */
    void switchLamp(int);
    /**
     * @brief changeValueDimmer
     * @param id: number of the dimmer
     * @param val: target value
     */
    void changeValueDimmer(int id, int val);
    /**
     * @brief changeStateSiren
     * @param id: number of the siren
     * @param siren: state of the siren (light+sound)
     * @param lamp: state of the light only (if siren=false)
     */
    void changeStateSiren(int id, bool siren, bool lamp=false);
    /**
     * @brief refresh: checks the state of the network
     * - Emits update(true, t, n) if the nth device of type t state differs from
     * what was known (see first commentary of this file). The actual state is
     * saved and can be acquired thanks to getSateOf...()
     * - Emits update(false, default, default) if something else changed, such
     * as the name of a device... To really recreate the network if it has been
     * modified, connection(QString) should be called
     * - Emits sensorTripped() if a sensor is (un)tripped
     * - setTimer(on, T) will call refresh() every T msec
     */
    void refresh();

    /**
     * @brief alert: different coordinate actions (switch all the lights on...)
     * depending on the level of alert.
     * This is useful to have an alert button in a GUI for example
     * Keep in mind that this only work when your soft is running! For automatic
     * alerts (like fire detected => switch on sirens) use Vera scenes which
     * will run as long as the Vera is on.
     */
    void alert(int level=1);
    void changeValueBlind(int id, int val);


private slots:
    void sslErrors(QNetworkReply*, QList<QSslError>);
    void createNetwork(QNetworkReply*);
    void updateNetwork(QNetworkReply*);

signals:
    /**
     * @brief connectionStatus: see connection()
     */
    void connectionStatus(bool);
    /**
     * @brief update: see refresh
     */
    void update(bool ok, Vera::Type d=Vera::none, int id=0);
    /**
     * @brief sensorTripped: see refresh
     */
    void sensorTripped(bool on, int num);

private:
    QNetworkAccessManager *netManager;

    std::vector<Vera::BinaryLight> lights;
    std::vector<Vera::Dimmer> dimmers;
    std::vector<Vera::Sensor> sensors;
    std::vector<Vera::Siren> sirens;
    std::vector<Vera::Blind> blinds;

    QString server;

    QTimer *refreshTimer;
    int timerPeriod;

};

#endif // VERACONTROLLER_H
