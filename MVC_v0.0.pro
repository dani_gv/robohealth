#-------------------------------------------------
#
# Project created by QtCreator 2017-04-06T19:51:58
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MVC_v0
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        principal.cpp \
    configuracion.cpp \
    modelo.cpp \
    bbdd.cpp \
    eyex.cpp \
    conexion.cpp \
    veracontroller.cpp \
    VeraDevices.cpp \
    miboton.cpp \
    sensores.cpp \
    actuadores.cpp \
    mislider2.cpp \
    cuadrotexto.cpp \
    multimedia.cpp

HEADERS  += principal.h \
    configuracion.h \
    modelo.h \
    bbdd.h \
    eyex.h \
    conexion.h \
    request.h \
    veracontroller.h \
    VeraDevices.h \
    miboton.h \
    sensores.h \
    actuadores.h \
    mislider2.h \
    cuadrotexto.h \
    multimedia.h

win32: LIBS += -L$$PWD/Componentes/lib/ -lTobii.EyeX.Client

INCLUDEPATH += $$PWD/Componentes
DEPENDPATH += $$PWD/Componentes

RESOURCES += \
    iconos.qrc
