#ifndef SENSORES_H
#define SENSORES_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QGraphicsView>
#include <QVector>
#include <QString>

#include "miboton.h"
#include "modelo.h"

using namespace std;

class Modelo;

class Sensores : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Sensores: Constructor que crea el layout de la ventana, asignándolo
     * a un puntero a QGraphicsView para poder mostrarlo.
     * @param parent
     */
    explicit Sensores(QWidget *parent = 0);
    ~Sensores();

    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void setModel(Modelo &m);
    /**
     * @brief showMaximized: Muestra la ventana, a pantalla completa
     */
    void showMaximized();
    /**
     * @brief hide: Esconde la ventana.
     */
    void hide();
    /**
     * @brief MostrarDispositivo: Coloca el botón pasado por parámetro en el layout
     * @param b: Botón recibido desde el modelo
     */
    void MostrarDispositivos(miBoton *b);
    /**
     * @brief LimpiarDispositivo: Elimina del layout el botón pasado por parámetro.
     * @param b: Botón recibido desde el modelo.
     */
    void LimpiarDispostivos(miBoton *b);
    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param str: hoja de estilos recibida.
     */
    void styleSheet(QString str);

private:
    Modelo *miModelo;

    QGraphicsView *vista;
    QGridLayout *layout;

    const int maximoBotonesFila = 3;
    int fila;
    int columna;

    miBoton *volver;


signals:

public slots:
    /**
     * @brief Volver: Slot llamado por el botón volver que regresa a la pantalla principal.
     */
    void Volver();

};

#endif // SENSORES_H
