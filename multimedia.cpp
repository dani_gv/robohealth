#include "multimedia.h"

Multimedia::Multimedia(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();

    layout->setSpacing(150);

    volver = new miBoton("Volver");
    connect(volver, SIGNAL(clicked(bool)), this, SLOT(Volver()));


}

Multimedia::~Multimedia()
{

}

void Multimedia::setModel(Modelo &m)
{
    miModelo = &m;
    connect(volver, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
}

void Multimedia::stylesheet(QString str)
{
    vista->setStyleSheet(str);
}

void Multimedia::cambioVentana()
{

}

void Multimedia::Volver()
{

}
