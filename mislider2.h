#ifndef MISLIDER2_H
#define MISLIDER2_H

/**
*** Clase creada para sustituir al QSlider proporcionado por QtCreator.
***
*** Sólo se llama cuando se tiene que crear un Slider para una luz de intensidad variable. Esto se hace
*** porque, para que el usuario pueda interaccionar con estos dispositivos empleando sus ojos, es necesario
*** sustituir el slider típico por un layout horizontal con un botón de "menos", una etiqueta del nombre, y un
*** botón de "más". De esta manera, el usuario puede variar la intensidad del dimmer pulsando estos botones
**/

#include <QObject>
#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QHBoxLayout>
#include <QGroupBox>

#include "miboton.h"

class miSlider2 : public QSlider
{
    Q_OBJECT
public:
    /**
     * @brief miSlider2: Constructor que creará el layout horizontal formado por los dos botones
     * y la etiqueta especificadas.
     * @param title: Texto que mostrará la etiqueta.
     */
    miSlider2(QString title);

    /**
     * @brief getSlider
     * @return Devuelve el widget que contiene
     * el layout horizontal creado para que se pueda colocar en cualquier otro layout.
     */
    QWidget* getSlider();

    /**
     * @brief getIntensidad
     * @return
     */
    int getIntensidad();
    /**
     * @brief setIntensidad: Establece la intensidad del dimmer.
     * @param value: valor de la intensidad a establecer.
     */
    void setIntensidad(int value);
    /**
     * @brief getTitulo
     * @return Devuelve el texto que tiene la etiqueta.
     */
    QString getTitulo();

public: Q_SIGNALS:
    /**
     * @brief hover: Señal emitida cuando el cursor se encuentra dentro de un botón perteneciente al Slider
     * @param b: botón dentro del que se encuentra.
     */
    void hover(miBoton *b);
    /**
     * @brief clickado: Señal emitida cuando se produce un pulsa el botón de más o de menos perteneciente
     * al Slider. Sirve para comunicarle al modelo que el valor de la intensidad ha cambiado.
     */
    void clickado(miSlider2 *);

public slots:
    /**
     * @brief hovered: Slot al que se entra cuando el cursor se encuentra dentro de algún botón.
     */
    void hovered(miBoton *);
    /**
     * @brief aumentarIntensidad: Slot llamado cuando se pulsa el botón más, que aumentará la intensidad en un valor de 10
     * y avisará al modelo del cambio mediante la señal "Clickado"
     */
    void aumentarIntensidad();
    /**
     * @brief disminuirIntensidad: Slot que realiza la misma función que aumentarIntensidad pero para disminuirla.
     */
    void disminuirIntensidad();

private:
    miBoton *mas;
    miBoton *menos;

    QLabel *titulo;
    QLabel *intens;

    QHBoxLayout *layout;

    int intensidad;

    QGroupBox *encuadre;
    QWidget *widget;

};

#endif // MISLIDER2_H
