#include "cuadrotexto.h"

CuadroTexto::CuadroTexto(QWidget *parent) : QWidget(parent)
{
    aceptarButton = new miBoton("Aceptar");
    connect(aceptarButton, SIGNAL(clicked(bool)), this, SLOT(Aceptar()));

    informacion = new QLabel();
    vista = new QGraphicsView();
    layout = new QVBoxLayout();

    layout->addWidget(informacion);
    layout->addWidget(aceptarButton);

    vista->setMinimumSize(600,400);
    vista->setLayout(layout);
}

CuadroTexto::~CuadroTexto()
{

}

void CuadroTexto::setTexto(QString str)
{
    informacion->setText(str);
}

void CuadroTexto::show()
{
    vista->show();
}

void CuadroTexto::setModel(Modelo &m)
{
    miModelo = &m;
    connect(aceptarButton, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
}

void CuadroTexto::styleSheet(QString style)
{
    vista->setStyleSheet(style);
}

void CuadroTexto::Aceptar()
{
    vista->hide();
   // miModelo->habilitarVentanas();
}

