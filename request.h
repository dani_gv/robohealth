#ifndef REQUEST_H
#define REQUEST_H

// JSON Parsing strings
#define J_DEVICES ("devices")
#define J_CATEGORY ("category_num")
#define CAT_BINLIGHT (3)
#define CAT_DIMMER (2)
#define CAT_SENSOR (4)
#define CAT_BLIND (8)
#define CAT_SIRENS (24)
#define J_ID ("id")
#define J_NAME ("name")
#define J_STATES ("states")
#define STA_VAR ("variable")
#define STA_VAL ("value")
#define VAR_STATUS ("Status")
#define VAR_DIMMERSTATUS ("LoadLevelStatus")
#define VAR_SENSORARMED ("Armed")
#define VAR_SENSORTRIPPED ("Tripped")

// Requests strings
#define R_DATA ("/data_request?id=user_data&output_format=json")
#define R_ACTION ("/data_request?id=action&output_format=json&DeviceNum=")
#define R_LIGHT ("&serviceId=urn:upnp-org:serviceId:SwitchPower1&action=SetTarget&newTargetValue=")
#define R_DIMMER ("&serviceId=urn:upnp-org:serviceId:Dimming1&action=SetLoadLevelTarget&newLoadlevelTarget=")
#define R_BLIND ("&serviceId=urn:upnp-org:serviceId:Dimming1&action=SetLoadLevelTarget&newLoadlevelTarget=")


#endif // REQUEST_H
