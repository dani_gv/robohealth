#ifndef BBDD_H
#define BBDD_H

/**
*** Clase que tiene conexión con el modelo y que sirve para establecer y consultar
*** datos, como los tiempos de guiño, los usuarios disponibles, etc...). Es consultada
*** directamente por el modelo para establecer acciones en función de lo que esté guardado en
*** la BBDD.
***/

#include <QVector>
#include <QString>
#include <iostream>

using namespace std;

class BBDD
{
public:

    BBDD();

    //ACCIONES SOBRE USUARIOS TOBII
    void EliminarUsuario (QString user);
    void setUsuarioActivo (QString user);
    QString getUsuarioActivo();
    void setUsuarios(vector<QString> users);
    vector<QString>GetUsuarios();

    //Modo de funcionamiento y Modo Click
    QString getModoActivo();
    void setModoActivo(QString str);
    QString getModoClick();
    void setModoClick(QString str);

    //Cambios en los Tiempos establecidos.
    void setTiempoCierre(int);
    void setTiempoGuino(int);
    void setTiempoFijacion(int);
    int getTiempoCierre();
    int getTiempoGuino();
    int getTiempoFijacion();


    //ACCIONES SOBRE FAVORITOS
    void AnyadirFavorito (QString fav);
    void EliminarFavorito (QString fav);
    QVector<QString> GetFavoritos();



    //ACCIONES SOBRE DEVICES
    void AnyadirDevice(QString dev);
    QVector<QString>GetDevices();


    //ACCIONES SOBRE PANTALLA
    void setPantallaActiva(QString str);
    QString getPantallaActiva();

private:
    QVector<QString>favoritos;
    QString usuarioActivo;
    QVector<QString>devices;
    QVector<QString>tipoDevices;
    vector<QString>usuarios;
    QString modoActivo;
    QString modoClick;
    QString pantallaActiva;

    int tiempoFijacion;
    int tiempoCierre;
    int tiempoGuino;

};

#endif // BBDD_H
