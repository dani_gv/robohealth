#include "mislider2.h"

miSlider2::miSlider2(QString title)
{
    titulo = new QLabel(title);
    intens = new QLabel();
    intens->setAlignment(Qt::AlignHCenter);
    QVBoxLayout *etiquetas = new QVBoxLayout();
    etiquetas->addWidget(titulo);
    etiquetas->addWidget(intens);

    encuadre = new QGroupBox("");

    mas = new miBoton("10% +");
    connect(mas, SIGNAL(hovered(miBoton*)), this, SLOT(hovered(miBoton*)));
    connect(mas, SIGNAL(clicked(bool)), this, SLOT(aumentarIntensidad()));

    menos = new miBoton("10% -");
    connect(menos, SIGNAL(hovered(miBoton*)), this, SLOT(hovered(miBoton*)));
    connect(menos, SIGNAL(clicked(bool)), this, SLOT(disminuirIntensidad()));

    layout = new QHBoxLayout();

    QHBoxLayout *aux = new QHBoxLayout();

    aux->addWidget(menos);
    aux->addLayout(etiquetas);
    aux->addWidget(mas);

    encuadre->setLayout(aux);

    titulo->setAlignment(Qt::AlignCenter);
    layout->addWidget(encuadre);

    widget = new QWidget();
    widget->setLayout(layout);
}

QWidget *miSlider2::getSlider()
{
    return widget;
}

int miSlider2::getIntensidad()
{
    return intensidad;
}

void miSlider2::setIntensidad(int value)
{
    intensidad = value;

    QString str;
    str += QString::number(intensidad) + " %";
    intens->setText(str);
}

QString miSlider2::getTitulo()
{
    return titulo->text();
}

void miSlider2::hovered(miBoton *boton)
{
    Q_EMIT hover(boton);
}

void miSlider2::aumentarIntensidad()
{
    if (intensidad <= 90)
        intensidad += 10;
    else
        intensidad = 100;

    QString str;
    str += QString::number(intensidad) + " %";
    intens->setText(str);

    Q_EMIT clickado(this);
}

void miSlider2::disminuirIntensidad()
{
    if (intensidad >= 10)
        intensidad -= 10;
    else
        intensidad = 0;

    QString str;
    str += QString::number(intensidad) + " %";
    intens->setText(str);

    Q_EMIT clickado(this);
}
