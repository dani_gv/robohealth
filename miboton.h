#ifndef MIBOTON_H
#define MIBOTON_H

/**
*** Clase que hereda del botón proporcionado por QtCreator
***
*** Se realiza esta clase para crear la señal Hovered, ya que no está creada por defecto.
*** De esta manera, cada vez que el cursor se encuentre encuadrado dentro de un botón que
*** pertenezca a la clase miBoton, se emite la señal Hovered que lleva, como parámetro, el
*** botón sobre el que está el cursor.
**/


#include <QObject>
#include <QPushButton>
#include <QString>
#include <QRect>
#include <QCursor>
#include <QDebug>
#include <QWidget>

class miBoton : public QPushButton
{
    Q_OBJECT
public:
    /**
     * @brief miBoton: Constructor que sólo asigna el texto introducido como parámetro.
     * @param name
     */
    miBoton(QString name);

    /**
     * @brief enterEvent: Método proporcionado por la clase QPushButton, de la que hereda miBoton,
     * que es llamado cada vez que el cursor entra en el botón.
     * @param event
     */
    virtual void enterEvent(QEvent *event);

public: Q_SIGNALS:
    void hovered(miBoton *);

};

#endif // MIBOTON_H
