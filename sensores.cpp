#include "sensores.h"

Sensores::Sensores(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();

    layout->setSpacing(150);

    columna = 0;
    fila = 1;

    volver = new miBoton("Volver");
   // volver->setStyleSheet("background-color: rgb(95,82,146)");
    connect(volver, SIGNAL(clicked()), this, SLOT(Volver()));


    layout->addWidget(volver, 0, 2, 1, 1);
    //SEÑAL HOVERED

    vista->setLayout(layout);
}

Sensores::~Sensores()
{

}

void Sensores::setModel(Modelo &m)
{
    miModelo = &m;
    connect(volver, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
}

void Sensores::showMaximized()
{
    vista->showMaximized();
}

void Sensores::hide()
{
    vista->hide();
}

void Sensores::MostrarDispositivos(miBoton *b)
{
    if (b->text() == "Alerta")
    {
        layout->addWidget(b, 0, 0, 1, 2);
        return;
    }
    if (columna >= maximoBotonesFila)
    {
        fila++;
        columna = 0;
    }
    layout->addWidget(b, fila, columna, 1, 1);
    b->setVisible(true);
    columna++;
}

void Sensores::LimpiarDispostivos(miBoton *b)
{
    layout->removeWidget(b);
    b->setVisible(false);

    fila = 1;
    columna = 0;
}

void Sensores::styleSheet(QString str)
{
    vista->setStyleSheet(str);
}

void Sensores::Volver()
{
    miModelo->CambioVentana("Principal");
}
