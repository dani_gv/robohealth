#ifndef MULTIMEDIA_H
#define MULTIMEDIA_H

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QGridLayout>

#include "modelo.h"
#include "miboton.h"

using namespace std;

class Modelo;

class Multimedia : public QWidget
{
    Q_OBJECT
public:
    explicit Multimedia(QWidget *parent = 0);
    ~Multimedia();

    void setModel(Modelo &m);
    void showMaximized();
    void hide();

    void stylesheet(QString str);

private:
    miBoton *imagenes;
    miBoton *sonidos;
    miBoton *videos;

    miBoton *volver;

    Modelo *miModelo;

    QGraphicsView *vista;
    QGridLayout *layout;

signals:

public slots:

private slots:
    void cambioVentana();
    void Volver();
};

#endif // MULTIMEDIA_H
