#include "conexion.h"

Conexion::Conexion(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();

    ip = new QLineEdit("http://138.100.100.124:3480");
    Conectar = new QPushButton("Conectar");
    connect(Conectar, SIGNAL(clicked(bool)), this, SLOT(urlEdited()));

    layout->addWidget(ip, 0, 0, 1, 1);
    layout->addWidget(Conectar, 0, 1, 1, 1);

    vista->setLayout(layout);
    vista->setMinimumSize(600,400);
}

void Conexion::SetModel(Modelo &m)
{
    miModelo = &m;
}

void Conexion::show()
{
    vista->show();
}

void Conexion::hide()
{
    vista->hide();
}

void Conexion::styleSheet(QString str)
{
    vista->setStyleSheet(str);
}

void Conexion::urlEdited()
{
    QString url = ip->text();

    if (QUrl(url).isValid())
        miModelo->connection(url);
}
