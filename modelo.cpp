#include "modelo.h"

Modelo::Modelo(Principal &p, Configuracion &conf, Conexion &conex, Sensores &sens, Actuadores &act, CuadroTexto &cuad, QObject *parent) : QObject(parent)
{
    ojoscerrados = false;

    principal = &p;
    configuracion = &conf;
    conexion = &conex;
    sensor = &sens;
    actuador = &act;
    cuadro = &cuad;

    alerta = new miBoton("Alerta");
    connect(alerta, SIGNAL(hovered(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));

    bbdd = new BBDD();
    bbdd->setModoActivo("Tactil");
    bbdd->setModoClick("Fijacion");

    controller = new veraController();
    connect(controller, SIGNAL(connectionStatus(bool)), this, SLOT(connectionReply(bool)));
    connect(controller, SIGNAL(sensorTripped(bool,int)), this, SLOT(sensorSignalReceived(bool,int)));
    connect(alerta, SIGNAL(clicked(bool)), controller, SLOT(alert()));

    Clickado = false;
    eye = new EyeX();
    connect(eye, SIGNAL(SenyalUsuarioActual(QString)), this, SLOT(setUsuarioActual(QString)));
    connect(eye, SIGNAL(ListaUsuarios(vector<QString>)), this, SLOT(ActualizarUsuarios(vector<QString>)));
    connect(eye, SIGNAL(Posicionar(float,float)), this, SLOT(Posicionar(float,float)));
    connect(eye, SIGNAL(GuinoDerecho()), this, SLOT(Guino_ojo()));
    connect(eye, SIGNAL(GuinoIzquierdo()), this, SLOT(Guino_ojo()));
    connect(eye, SIGNAL(cierreOjos()), this, SLOT(Cierre()));
    connect(eye, SIGNAL(ojosAbiertos()), this, SLOT(ojosAbiertos()));


    stylesheet = "QWidget {background-color: rgb(46,69,113);"
                                            "color: white;}"
                 "QPushButton { font-size: 20pt;}"
                 "QLabel { font-size: 20pt;}"
                 "miBoton {background-color: rgb(237,155,80);"
                                            "color: black; "
                                            "font-size: 60pt; }"
                 "QGroupBox { font-size: 30pt;}"
                 "miBoton#luz  { background-color: rgb(95,81,145);"
                                             "color: white;}"
                 "miBoton#sirena { background-color: rgb(39,26,89);"
                                             "color: white;}"
                 "*[sensor='on'] { background-color: rgb(180,27,15);"
                                    "color: white; }"
                 "*[sensor='off'] { background-color: rgb(11,98,110);"
                                    "color: white; }"
                 "*[sensor='tripped'] { background-color: rgb(11,130,59);"
                                        "color: black; }";

    CambioVentana("Conexion");

    automaticDecoTimer = new QElapsedTimer();

}

void Modelo::CrearUsuario()
{
    eye->CrearUsuario();
}

void Modelo::EliminarUsuario(QString str)
{
    eye->EliminarUsuario(str);
}

void Modelo::RecalibrarUsuario()
{
    eye->Recalibrar();
}

void Modelo::ComprobarCalibracion()
{
    eye->ComprobarCalibracion();
}

void Modelo::CambiarUsuario(QString str)
{
    eye->CambiarUsuario(str);
}

void Modelo::CambiarModoActivo(QString str)
{
    bbdd->setModoActivo(str);
    if (str == "Ojos")
    {
        eye->IniciarPrograma();
        return;
    }
    else if (str == "Tactil")
    {

    }
    else if(str == "Voz")
    {

    }
    else if (str == "Gestos")
    {

    }
}

QString Modelo::getModoActivo()
{
    return (bbdd->getModoActivo());
}

void Modelo::AnyadirFavorito(QString str)
{
    bbdd->AnyadirFavorito(str);
}

void Modelo::EliminarFavorito(QString str)
{
    for (int i = 0; i < bbdd->GetFavoritos().size(); i++)
    {
        QString str = bbdd->GetFavoritos()[i];
        for (int j = 0; j < luces.size(); j++)
            if (luces[j]->text() == str)
            {
                principal->EliminarBotones(luces[j]);
                break;
            }
        for (int j = 0; j < sirenas.size(); j++)
            if (sirenas[j]->text() == str)
            {
                principal->EliminarBotones(sirenas[j]);
                break;
            }
        for (int j = 0; j < sensores.size(); j++)
            if (sensores[j]->text() == str)
            {
                principal->EliminarBotones(sensores[j]);
                break;
            }        
    }
    bbdd->EliminarFavorito(str);
}

QVector<QString> Modelo::GetFavoritos()
{
    return (bbdd->GetFavoritos());
}

QVector<QString> Modelo::GetDevices()
{
    return (bbdd->GetDevices());
}

void Modelo::CambioVentana(QString str)
{
    if (str == "Conexion")
    {
        principal->hide();
        configuracion->hide();
        conexion->show();
        sensor->hide();
        actuador->hide();
    }
    else if (str == "Principal")
    {
        conexion->hide();
        configuracion->hide();
        sensor->hide();
        actuador->hide();
        principal->cambioModo(bbdd->getModoClick());
        principal->clickarBoton(bbdd->getModoActivo());
        principal->showMaximized();

        for (int i = 0; i < sensores.size(); i++)
            sensor->LimpiarDispostivos(sensores[i]);
        for (int i = 0; i < luces.size(); i++)
            actuador->LimpiarDispositivo(luces[i]);
        for (int i = 0; i < sirenas.size(); i++)
            actuador->LimpiarDispositivo(sirenas[i]);
        for (int i = 0; i < sliders.size(); i++)
            actuador->EliminarDimmer(sliders[i]->getSlider());
        for (int i = 0; i < blinds.size(); i++)
            actuador->EliminarDimmer(blinds[i]->getSlider());


        principal->DibujarBotones(alerta);
        for (int i = 0; i < bbdd->GetFavoritos().size(); i++)
        {
            QString str = bbdd->GetFavoritos()[i];
            for (int j = 0; j < luces.size(); j++)
                if (luces[j]->text() == str)
                {
                    principal->DibujarBotones(luces[j]);
                    break;
                }

            for (int j = 0; j < sirenas.size(); j++)
                if (sirenas[j]->text() == str)
                {
                    principal->DibujarBotones(sirenas[j]);
                    break;
                }

            for (int j = 0; j < sensores.size(); j++)
                if (sensores[j]->text() == str)
                {
                    principal->DibujarBotones(sensores[j]);
                    break;
                }
            for (int j = 0; j < sliders.size(); j++)
                if (sliders[j]->getTitulo() == str)
                {
                    principal->DibujarDimmer(sliders[j]->getSlider());
                    break;
                }
            for (int j = 0; j < blinds.size(); j++)
                if (blinds[j]->getTitulo() == str)
                {
                    principal->DibujarDimmer(blinds[j]->getSlider());
                    break;
                }

        }
    }
    else if (str == "Configuracion")
    {
        conexion->hide();
        configuracion->SetUsuarios(bbdd->GetUsuarios());
        principal->hide();
        actuador->hide();
        configuracion->showMaximized();
        bbdd->setModoActivo("Tactil");
    }
    else if (str == "Sensores")
    {
        principal->hide();
        configuracion->hide();
        conexion->hide();
        actuador->hide();

        sensor->MostrarDispositivos(alerta);
        for (int i = 0; i < sensores.size(); i++)
            sensor->MostrarDispositivos(sensores[i]);

        sensor->showMaximized();
    }
    else if (str == "Actuadores")
    {
        principal->hide();
        configuracion->hide();
        conexion->hide();
        sensor->hide();

        actuador->MostrarDispositivo(alerta);
        for (int i = 0; i < luces.size(); i++)
            actuador->MostrarDispositivo(luces[i]);
        for (int i = 0; i < sirenas.size(); i++)
            actuador->MostrarDispositivo(sirenas[i]);
        for (int i = 0; i < sliders.size(); i++)
            actuador->MostrarDimmer(sliders[i]->getSlider());
        for (int i = 0; i < blinds.size(); i++)
            actuador->MostrarDimmer(blinds[i]->getSlider());

        actuador->showMaximized();
    }
    else if (str == "Multimedia")
    {
        //Ventana con imágenes que puedan ir pasando cuando le damos a un botón
    }
    else
        return;
    bbdd->setPantallaActiva(str);
    establecerStyleSheet();
    return;
}

void Modelo::TiempoFijacion(int time)
{
    bbdd->setTiempoFijacion(time);
}

void Modelo::TiempoGuino(int time)
{
    eye->CambiarTiempoGuino(time);
    bbdd->setTiempoGuino(time);
}

void Modelo::TiempoCierre(int time)
{
    eye->CambiarTiempoCierre(time);
    bbdd->setTiempoCierre(time);
}

void Modelo::setModoClick(QString mode)
{
    bbdd->setModoClick(mode);
}

void Modelo::ColocarMouse(float x, float y)
{
    if (getModoActivo() == "Ojos")
    {
        qDebug()<<"Colocar Mouse";
        if (mouseHabilitation == false)
        {
            QCursor::setPos(xCentro, yCentro);
            tFinal = QTime::currentTime();
            if (x < x1 || x > x2 || y < y1 || y > y2)
            {
                mouseHabilitation = true;
                QCursor::setPos(x, y);
                Clickado = false;
                return;
            }
            else if (Clickado && bbdd->getModoClick()=="Fijacion")
                if(ComprobacionTiempos(tInicial, tFinal, bbdd->getTiempoFijacion()))
                {
                    qDebug()<<"Fijacion";
                    QCursor::setPos(x, y);
                    Click();
                    Clickado = false;
                }

        }
        else
            QCursor::setPos(x, y);
    }
}

void Modelo::connection(QString str)
{
    controller->connection(str);
}

void Modelo::CreateDevices()
{
    lucesMapper = new QSignalMapper();
    sirenasMapper = new QSignalMapper();
    //First clear old buttons
    clear();

    /* ******** Sirens ******** */
    int n = controller->getNumberOf(Vera::siren);
    for (int i = 0; i < n; i++)
    {
        miBoton *aux = new miBoton(controller->getNameOf(Vera::siren, i));
        aux->setObjectName("sirena");
        connect(aux, SIGNAL(hovered(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));
        sirenas.push_back(aux);
        aux->setCheckable(true);
        sirenasMapper->setMapping(aux, i);
        connect(aux, SIGNAL(clicked(bool)), sirenasMapper, SLOT(map()));
        aux->setChecked(controller->getStateOfSiren(i, true));

        bbdd->AnyadirDevice(aux->text());
    }
    connect(sirenasMapper, SIGNAL(mapped(int)), this, SLOT(switchSiren(int)));

    n = controller->getNumberOf(Vera::binLight);
    for (int i = 0; i < n; i++)
    {
        miBoton *aux = new miBoton(controller->getNameOf(Vera::binLight, i));
        aux->setObjectName("luz");
        connect(aux, SIGNAL(hovered(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));
        luces.push_back(aux);
        aux->setCheckable(true);
        lucesMapper->setMapping(aux,i);
        connect(aux, SIGNAL(clicked(bool)), lucesMapper, SLOT(map()));
        aux->setChecked(controller->getStateOfLamp(i));
        if(controller->getStateOfLamp(i) == true)
        {
            aux->setIcon(QIcon(":/Imagenes/luzEncendidaP.ico"));
            aux->setIconSize(QSize(tamanoIcono,tamanoIcono));
        }
        else
        {
            aux->setIcon(QIcon(":/Imagenes/luzApagada.jpg"));
            aux->setIconSize(QSize(tamanoIcono,tamanoIcono));
        }

        bbdd->AnyadirDevice(aux->text());
    }
    connect(lucesMapper, SIGNAL(mapped(int)), controller, SLOT(switchLamp(int)));

    /* *********** Dimmers ***************** */
    n = controller->getNumberOf(Vera::dimmer);
    for (int i = 0; i < n; i++)
    {
        miSlider2 *aux = new miSlider2(controller->getNameOf(Vera::dimmer, i));
        aux->setIntensidad(controller->getStateOfDimmer(i));
        aux->setObjectName("slider");
        connect(aux, SIGNAL(hover(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));
        connect(aux, SIGNAL(clickado(miSlider2*)), this, SLOT(cambioValorDimmer(miSlider2*)));
        sliders.push_back(aux);

        bbdd->AnyadirDevice(controller->getNameOf(Vera::dimmer, i));
    }

    n = controller->getNumberOf(Vera::sensor);
    for (int i = 0; i < n; i++)
    {
        miBoton *aux = new miBoton(controller->getNameOf(Vera::sensor, i));
        aux->setObjectName("sensor");
        connect(aux, SIGNAL(hovered(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));
        aux->setEnabled(false);

        if (!(controller->getStateOfSensor(i)))
            aux->setProperty("sensor", "off");
        else if (controller->getSensorTripped(i))
            aux->setProperty("sensor", "tripped");
        else
            aux->setProperty("sensor", "on");

        sensores.push_back(aux);
        bbdd->AnyadirDevice(aux->text());
    }

    /* *********** Blinds ***************** */
    n = controller->getNumberOf(Vera::blind);
    for (int i = 0; i < n; i++)
    {
        miSlider2 *aux = new miSlider2(controller->getNameOf(Vera::blind, i));
        int intensidad = controller->getStateOfBlind(i);
        aux->setIntensidad(intensidad);
        aux->setObjectName("slider");
        connect(aux, SIGNAL(hover(miBoton*)), this, SLOT(Colocacion_Raton(miBoton*)));
        connect(aux, SIGNAL(clickado(miSlider2*)), this, SLOT(cambioValorBlind(miSlider2*)));
        blinds.push_back(aux);

        bbdd->AnyadirDevice(controller->getNameOf(Vera::blind, i));
    }
    establecerStyleSheet();
}

void Modelo::UpdateDevices(bool ok, Vera::Type d, int id)
{

    if (!ok) //The network structure changed or the controller doesn't answer
    {
        //Start the dimmer and disable the window
        if (!(automaticDecoTimer->isValid()))
        {
            habilitacion(false);
            automaticDecoTimer->start();
        }
        else if (automaticDecoTimer->elapsed() >= TIME_DECO)
        {
            controller->setTimer(false);
            clear();
            habilitacion(true);
        }
        return;
    }

    //Re-enabled the window
    if (automaticDecoTimer->isValid())
    {
        habilitacion(true);
        automaticDecoTimer->invalidate();
    }

    //update the display
    switch (d)
    {
    case Vera::siren:
        sirenas[id]->setChecked(controller->getStateOfSiren(id, true));
        break;
    case Vera::binLight:
        luces[id]->setChecked(controller->getStateOfLamp(id));
        if(controller->getStateOfLamp(id) == true)
        {
            luces[id]->setIcon(QIcon(":/Imagenes/luzEncendida.jpg"));
            luces[id]->setIconSize(QSize(tamanoIcono,tamanoIcono));
        }
        else
        {
            luces[id]->setIcon(QIcon(":/Imagenes/luzApagada.jpg"));
            luces[id]->setIconSize(QSize(tamanoIcono,tamanoIcono));
        }
        break;
    case Vera::dimmer:
        //
        break;
    case Vera::sensor:
        if (!controller->getStateOfSensor(id))
            sensores[id]->setProperty("sensor", "off");
        else if (controller->getSensorTripped(id))
            sensores[id]->setProperty("sensor", "tripped");
        else
            sensores[id]->setProperty("sensor", "on");
        break;
    }

    establecerStyleSheet();
}

void Modelo::connectionReply(bool ok)
{
    if (ok)
    {
        CreateDevices();
        controller->setTimer(true);
        connect(controller, SIGNAL(update(bool,Vera::Type,int)), this, SLOT(UpdateDevices(bool,Vera::Type,int)));
        CambioVentana("Principal");
    }
}

void Modelo::sensorSignalReceived(bool on, int id)
{
    if (on)
        sensores[id]->setProperty("sensor", "tripped");

    else
        sensores[id]->setProperty("sensor", "on");

    establecerStyleSheet();
}

void Modelo::cambioValorDimmer(miSlider2 *dimmer)
{
    for (int i = 0; i < sliders.size(); i++)
    {
        if (sliders[i]->getTitulo() == dimmer->getTitulo())
            controller->changeValueDimmer(i, dimmer->getIntensidad());
    }
}

void Modelo::cambioValorBlind(miSlider2 *blind)
{
    for (int i = 0; i < blinds.size(); i++)
    {
        if (blinds[i]->getTitulo() == blind->getTitulo())
            controller->changeValueBlind(i, blind->getIntensidad());
    }
}

void Modelo::ojosAbiertos()
{
    QTime prueba2;
    prueba2 = QTime::currentTime();

    float tiempo1 = (prueba.hour()*3600 + prueba.minute()*60 + prueba.second())*1000 + prueba.msec();
    float tiempo2 = (prueba2.hour()*3600 + prueba2.minute()*60 + prueba2.second())*1000 + prueba2.msec();

    if ((tiempo2 - tiempo1) >= 200)
        ojoscerrados = false;
}

void Modelo::clear()
{
    //Alert Button

    //Sirens
    while(sirenas.size() > 0)
    {
        delete sirenas.back();
        sirenas.pop_back();
    }
    delete sirenasMapper;
    sirenasMapper = new QSignalMapper(this);

    // Luces
    while(luces.size() > 0)
    {
        delete luces.back();
        luces.pop_back();
    }
    delete lucesMapper;
    lucesMapper = new QSignalMapper(this);

    // Sensores
    while (sensores.size() > 0)
    {
        delete sensores.back();
        sensores.pop_back();
    }
}

void Modelo::crearCuadro(QString inf)
{
    cuadro->show();
    cuadro->setTexto(inf);
    cuadro->styleSheet(stylesheet);
}

void Modelo::switchSiren(int num)
{
    controller->changeStateSiren(num, false, sirenas[num]->isChecked());
}

void Modelo::Click()
{
    QPoint p = QCursor::pos();
    if (p.x() == 0 || p.y() == 0)
        QCursor::setPos(xCentro, yCentro);

    qDebug()<<"Click";
    if (!ojoscerrados)
        focus->animateClick();
}

bool Modelo::ComprobacionTiempos(QTime tInicial, QTime tFinal, int tFijacion)
{
    int t1 = (tInicial.hour()*3600 + tInicial.minute()*60 + tInicial.second())*1000 + tInicial.msec();
    int t2 = (tFinal.hour()*3600 + tFinal.minute()*60 + tFinal.second())*1000 + tFinal.msec();

    if (t2-t1 >= tFijacion)
        return true;
    return false;
}

void Modelo::establecerStyleSheet()
{
    QString str = bbdd->getPantallaActiva();

    if (str == "Principal")
        principal->styleSheet(stylesheet);
    else if (str == "Configuracion")
        configuracion->styleSheet(stylesheet);
    else if (str == "Actuadores")
        actuador->styleSheet(stylesheet);
    else if (str == "Sensores")
        sensor->styleSheet(stylesheet);
    else if (str == "Conexion")
        conexion->styleSheet(stylesheet);
    else
        return;
}

void Modelo::habilitacion(bool activo)
{
    QString str = bbdd->getPantallaActiva();

    if (str == "Principal")
        principal->setEnabled(activo);
    else if (str == "Configuracion")
        configuracion->setEnabled(activo);
    else if (str == "Actuadores")
        actuador->setEnabled(activo);
    else if (str == "Sensores")
        sensor->setEnabled(activo);
    else if (str == "Conexion")
        conexion->setEnabled(activo);
    else
        return;
}

void Modelo::ActualizarUsuarios(vector<QString> lista)
{
    configuracion->SetUsuarios(lista);
    bbdd->setUsuarios(lista);
}

void Modelo::setUsuarioActual(QString str)
{
    bbdd->setUsuarioActivo(str);
    configuracion->SetUsuarioActivo(str);
}

void Modelo::Posicionar(float x, float y)
{
    if (bbdd->getModoActivo() == "Ojos")
    {
        qDebug()<<"Posicionar";
        QCursor::setPos(x,y);
        ColocarMouse(x,y);
    }
}

void Modelo::Guino_ojo()
{
    if (getModoActivo() == "Ojos" && Clickado && bbdd->getModoClick()=="Guino")
    {
        QCursor::setPos(xCentro, yCentro);
        Click();
        Clickado = false;
    }
}

void Modelo::Cierre()
{
    if(ojoscerrados)
        return;

    prueba = QTime::currentTime();
    QString str = bbdd->getModoClick();
    if (str == "Fijacion")
    {
        bbdd->setModoClick("Guino");
        principal->cambioModo("Guiño");
        qDebug()<<"Modo Guiño!";
    }
    else if (str == "Guino")
    {
        bbdd->setModoClick("Fijacion");
        principal->cambioModo("Fijacion");
        qDebug()<<"Modo Fijacion!";
    }
    else
        return;
    ojoscerrados = true;
}

void Modelo::Colocacion_Raton(miBoton *b)
{
    focus = b;
    nombreBoton = b->text();
    if (getModoActivo() == "Ojos")
    {
        qDebug()<<"Colocacion Raton" + b->text();
        Clickado = true;
        tInicial = QTime::currentTime();
        mouseHabilitation = false;

        QPoint p;
        p = b->mapFromGlobal(QPoint(0,0));

        xCentro = -p.x() + b->width()/2;
        yCentro = -p.y() + b->height()/2;

        x1 = xCentro - b->width()/2 - margenPixelBotones;
        y1 = yCentro - b->height()/2 - margenPixelBotones;
        x2 = x1 + b->width() + 2*margenPixelBotones;
        y2 = y1 + b->height() + 2*margenPixelBotones;

    }
}
