#include "actuadores.h"

Actuadores::Actuadores(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();

    //layout->setSpacing(150);

    columna = 0;
    fila = 1;

    volver = new miBoton("Volver");
    //volver->setStyleSheet("background-color: rgb(95,82,146)");
    connect(volver, SIGNAL(clicked()), this, SLOT(Volver()));


    layout->addWidget(volver, 0, 2, 1, 1);
    //SEÑAL HOVERED

    vista->setLayout(layout);
}

Actuadores::~Actuadores()
{

}

void Actuadores::setModel(Modelo &m)
{
    miModelo = &m;
    connect(volver, SIGNAL(hovered(miBoton*)), miModelo, SLOT(Colocacion_Raton(miBoton*)));
}

void Actuadores::showMaximized()
{
     vista->showMaximized();
}

void Actuadores::hide()
{
    vista->hide();
}

void Actuadores::MostrarDispositivo(miBoton *b)
{
    if (b->text() == "Alerta")
    {
        layout->addWidget(b, 0, 0, 1, 2);
        return;
    }
    if (columna >= maximoBotonesFila)
    {
        fila++;
        columna = 0;
    }
    layout->addWidget(b, fila, columna, 1, 1);
    b->setVisible(true);
    columna++;
}

void Actuadores::LimpiarDispositivo(miBoton *b)
{
    layout->removeWidget(b);
    b->setVisible(false);
/*
    if (columna > 0)
        columna--;
    else
    {
        columna = maximoBotonesFila;
        fila--;
    }
    */
}

void Actuadores::MostrarDimmer(QWidget *widget)
{
    if (columna >= maximoBotonesFila)
    {
        fila++;
        columna = 0;
    }
    layout->addWidget(widget, fila, columna, 1, 2);
    widget->setVisible(true);
    columna += 2;
}

void Actuadores::EliminarDimmer(QWidget *widget)
{
    layout->removeWidget(widget);
    widget->setVisible(false);
    fila = 1;
    columna = 0;
}

void Actuadores::styleSheet(QString str)
{
    vista->setStyleSheet(str);
}

void Actuadores::Volver()
{
     miModelo->CambioVentana("Principal");
}
