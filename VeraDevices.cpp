
#include "VeraDevices.h"

using namespace Vera;


/*
** Implementation of the mother Device class
*/

Device::Device() :
    id(0),
    name("Unknown device"),
    state(false)
{}

Device::Device(unsigned int i, std::string n, bool s) :
	id(i),
	name(n),
	state(s)
	{
    if (name.empty())
        name = "Device " ;//+ std::to_string(id);
    }

unsigned int Device::getId() const
{
	return id;
}

std::string Device::getName() const
{
	return name;
}

bool Device::getState() const
{
	return state;
}

Type Device::getType() const
{
	return none;
}

void Device::setId(unsigned int i)
{
	id=i;
}

void Device::setName(std::string n)
{
	name=n;
}

void Device::setState(bool s)
{
	state=s;
}

bool Device::changeState()
{
	state = !state;
	return state;
}

bool Device::sameDeviceAs(const Device d) const
{
	if (id == d.getId() && name.compare(d.getName()) == 0)
		return true;

	return false;
}

bool operator==(Device const& a, Device const& b)
{
	return a.sameDeviceAs(b);
}

bool operator!=(Device const& a, Device const& b)
{
	return !(a==b);
}

/*
** Implementation of the BinaryLight class
*/

BinaryLight::BinaryLight() :
    Device()
{}

BinaryLight::BinaryLight(unsigned int i, std::string n, bool s) :
	Device(i, n, s)
{}

// Overrride
Type BinaryLight::getType() const
{
  return binLight;
}

/*
** Implementation of the Dimmer class
*/

Dimmer::Dimmer() :
    Device(),
    level(0)
{}

Dimmer::Dimmer(unsigned int i, std::string n, unsigned int l) :
    Device(i, n, true),
    level(l)
{
	if (level>100)
		level=100;

	if (l==0)
        state=false;
}


unsigned int Dimmer::getLevel() const
{
	return level;
}

void Dimmer::setLevel(unsigned int l)
{
	level = l;
	if (level == 0)
		state = false;
	else
		state = true;
}

// Override
Type Dimmer::getType() const
{
	return dimmer;
}

/*
** Implementation of the Sensor class
*/

Sensor::Sensor() :
    Device(),
    tripped(false)
{}

Sensor::Sensor(unsigned int i, std::string n, bool s, bool t) :
	Device(i, n, s),
	tripped(t)
{}

bool Sensor::getTripped() const
{
	return tripped;
}

void Sensor::setTripped(bool t)
{
	tripped = t;
}

// Override
Type Sensor::getType() const
{
	return sensor;
}

/*
** Implementation of the Siren class
*/

Siren::Siren() :
    Device(),
    sirenOn(false)
{}

Siren::Siren(unsigned int i, std::string n, bool l, bool s) :
    Device(i, n, l),
    sirenOn(s)
{
    if (sirenOn)
        state = true;
}

bool Siren::getStateLamp()
{
    return state;
}

bool Siren::getStateSiren()
{
    return sirenOn;
}

void Siren::setLamp(bool l)
{
    if (!sirenOn)
        state = l;
}

void Siren::setSiren(bool s)
{
    sirenOn = s;
    state = s;
}

// Override
Type Siren::getType() const
{
    return siren;
}


Blind::Blind() :
    Device(),
    level(0)
{}

Blind::Blind(unsigned int i, std::string n, unsigned int l) :
    Device(i, n, true),
    level(l)
{
    if (level>100)
        level=100;

    if (l==0)
        state=false;
}

unsigned int Blind::getLevel() const
{
    return level;
}

void Blind::setLevel(unsigned int l)
{
    level = l;
}

Type Blind::getType() const
{
    return blind;
}
