#ifndef __VERA_DEVICES_H__
#define __VERA_DEVICES_H__

/**
*** Def of all the devices
*** Device is the mother class and the others are the daughters
*** 
*** The files names Device*.h refer to the original implementation
*** which as been united in one header file for easier use
**/


/*
** Device.h
*/

#include <string>

namespace Vera{

enum Type {none, binLight, dimmer, sensor, siren, blind};

class Device
{
public:
    Device();
    Device(unsigned int i, std::string n="", bool s=false);

	unsigned int getId() const;
	std::string getName() const;
	bool getState() const;
	Type getType() const;

	void setId(unsigned int i=0);
	void setName(std::string n="");
	void setState(bool s=false);

	bool changeState();
	bool sameDeviceAs(const Device) const;

protected:
	unsigned int id;
	std::string name;
	bool state;
};

/*
** DeviceBinaryLight.h
*/

class BinaryLight : public Device
{
public:
    BinaryLight();
	BinaryLight(unsigned int i, std::string n="", bool s=false);
	// A Binary Light is just a Device with Type binLight

    // Overrride
	Type getType() const;
};


/*
** DeviceDimmer.h
*/

class Dimmer : public Device
{
public:
    Dimmer();
	Dimmer(unsigned int i, std::string n="", unsigned int l=0);

	unsigned int getLevel() const;
	void setLevel(unsigned int l=0);

	// Overrride
    Type getType() const;

protected:
	unsigned int level; // Could use another type (short, char...) if memory was a
											// concern
};

/*
** DeviceSensor.h
*/

class Sensor : public Device
{
public:
    Sensor();
	Sensor(unsigned int i, std::string n="", bool s=false, bool t=false);

	bool getTripped() const;
	void setTripped(bool t=false);

	// Overrride
	Type getType() const;

protected:
	bool tripped;
};

/*
** DeviceSiren.h
*/

class Siren : public Device
{
public:
    Siren();
    Siren(unsigned int i, std::string n="", bool l=false, bool s=false);

    bool getStateLamp();
    bool getStateSiren();

    void setLamp(bool); // Only works if sirenOn==false (call setSiren first)
    void setSiren(bool);

    // Overrride
    Type getType() const;

protected:
    bool sirenOn; // light+sound. Compare with state for light only
};

/*
** DeviceBlind.h
*/

class Blind : public Device
{
public:
    Blind();
    Blind(unsigned int i, std::string n="", unsigned int l = 0);

    unsigned int getLevel() const;
    void setLevel(unsigned int l = 0);

    // Overrride
    Type getType() const;

protected:
    unsigned int level; // Could use another type (short, char...) if memory was a
                                            // concern
};
}
#endif
