#include "veracontroller.h" // see for doc.
#include <QtDebug>

veraController::veraController()
{
    netManager = new QNetworkAccessManager();
    QObject::connect(netManager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this, SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));

    refreshTimer = new QTimer(this);
    QObject::connect(refreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));

    timerPeriod = 0;
}

veraController::~veraController()
{
    delete netManager;
}

void veraController::connection(QString url)
{
    server = url;
    QUrl u(url + R_DATA);
    QNetworkRequest request(u);
    netManager->get(request);

    QObject::connect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(createNetwork(QNetworkReply*)));
}

void veraController::createNetwork(QNetworkReply *reply)
{
    // Clean
    lights.clear();
    dimmers.clear();
    sensors.clear();
    sirens.clear();
    blinds.clear();

    // Only for the request in the connect slot
    disconnect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(createNetwork(QNetworkReply*)));

    // Parse JSON
    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    if (doc.isNull()) {
        emit (connectionStatus(false));
        return;
    }

    QJsonObject obj = doc.object();

    // Create device vectors
    QJsonArray devices = obj.value(J_DEVICES).toArray(QJsonArray()); // Empty array by default

    for (int i=0; i<devices.size(); i++)
    {
        QJsonObject curDevice = devices.at(i).toObject();
        QJsonArray curStates = curDevice.value(J_STATES).toArray();

        int id, level;
        QString name;
        bool state, tripped;

        int cat = curDevice.value(J_CATEGORY).toInt(0);
        if (cat == 0)
            cat = curDevice.value(J_CATEGORY).toString().toInt();

        id = curDevice.value(J_ID).toInt(0); // id=0 by default
        name = curDevice.value(J_NAME).toString(); // "" by default

        QJsonObject curState;
        switch (cat)
        {
        case CAT_BINLIGHT: // binary light
            state=false;

            for (int j=0; j<curStates.size(); j++) {
                curState = curStates.at(j).toObject();
                if (curState.value(STA_VAR).toString() == VAR_STATUS) {

                    if (curState.value(STA_VAL).toString() == "1")
                        state = true;
                    else
                        state = false;

                    break; // for
                }
            }

            lights.push_back( Vera::BinaryLight(id, name.toStdString(), state) );
            break; // case

        case CAT_DIMMER: // dimmers
            level = 0;

            for (int j=0; j<curStates.size(); j++) {
                curState = curStates.at(j).toObject();
                if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                    level = curState.value(STA_VAL).toString("0").toInt();
                    break; // for
                }
            }

            dimmers.push_back( Vera::Dimmer(id, name.toStdString(), level) );
            break; // case

        case CAT_SENSOR: // sensors
            state = false; tripped = false;

            for (int j=0; j<curStates.size(); j++) {
                curState = curStates.at(j).toObject();

                if (curState.value(STA_VAR).toString() == VAR_SENSORARMED) {
                    if (curState.value(STA_VAL).toString() == "1")
                        state = true;
                    else
                        state = false;
                }

                if (curState.value(STA_VAR).toString() == VAR_SENSORTRIPPED) {
                    if (curState.value(STA_VAL).toString() == "1")
                        tripped = true;
                    else
                        tripped = false;
                }
            }

            sensors.push_back( Vera::Sensor(id, name.toStdString(), state, tripped) );
            break; // case

        case CAT_SIRENS: // sirens
            level = 0;

            for (int j=0; j<curStates.size(); j++) {
                curState = curStates.at(j).toObject();
                if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                    level = curState.value(STA_VAL).toString("0").toInt();
                    break; // for
                }
            }

            sirens.push_back( Vera::Siren(id, name.toStdString(), level>0, level>66) );
            break; // case

        case CAT_BLIND: //blinds
            level = 0;

            for (int j=0; j<curStates.size(); j++) {
                curState = curStates.at(j).toObject();
                if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                    level = curState.value(STA_VAL).toString("0").toInt();
                    break; // for
                }
            }

            blinds.push_back( Vera::Blind(id, name.toStdString(), level) );
            break; // case

        }
    }


    reply->deleteLater();
    emit (connectionStatus(true));
}

int veraController::getNumberOf(Vera::Type d)
{
    switch (d)
    {
    case Vera::binLight:
        return (int) lights.size();

    case Vera::dimmer:
        return (int) dimmers.size();

    case Vera::sensor:
        return (int) sensors.size();

    case Vera::siren:
        return (int) sirens.size();

    case Vera::blind:
        return (int) blinds.size();
    }
    return 0;
}

QString veraController::getNameOf(Vera::Type d, int id)
{
    switch (d)
    {
    case Vera::binLight:
        if (id < (int) lights.size())
            return QString::fromStdString( lights[id].getName() );
        break;

    case Vera::dimmer:
        if (id < (int) dimmers.size())
            return QString::fromStdString( dimmers[id].getName() );
        break;

    case Vera::sensor:
        if (id < (int) sensors.size())
            return QString::fromStdString( sensors[id].getName() );
        break;

    case Vera::siren:
        if (id < (int) sirens.size())
            return QString::fromStdString( sirens[id].getName() );
        break;
    case Vera::blind:
        if (id < (int) blinds.size())
            return QString::fromStdString( blinds[id].getName() );
        break;
    }
    return QString();
}

bool veraController::getStateOfLamp(int id)
{
    if (id < (int) lights.size())
        return lights[id].getState();

    return false;
}

void veraController::switchLamp(int num)
{
    QNetworkRequest request(switchLampUrl(num));
    netManager->get(request);

    lights[num].changeState();

    emit update(true, Vera::binLight, num);
    setTimer(refreshTimer->isActive(), timerPeriod);
}

QUrl veraController::switchLampUrl(const int num)
{
    if (num > (int) lights.size())
        return QUrl();

    QString answer;
    QTextStream(&answer) << server << R_ACTION << lights[num].getId() << R_LIGHT;

    if (lights[num].getState())
        answer.append("0");
    else
        answer.append("1");

    return QUrl(answer);
}

int veraController::getStateOfDimmer(int id)
{
    if (id < (int) dimmers.size())
        return dimmers[id].getLevel();

    return 0;
}

void veraController::changeValueDimmer(int id, int val)
{
    QNetworkRequest request(changeDimmerValueUrl(id, val));
    netManager->get(request);

    dimmers[id].setLevel(val);

    emit update(true, Vera::dimmer, id);
    setTimer(refreshTimer->isActive(), timerPeriod);
}

QUrl veraController::changeDimmerValueUrl(const int num, const int val)
{
    if (num > (int) dimmers.size())
        return QUrl();

    QString answer;
    QTextStream(&answer) << server << R_ACTION << dimmers[num].getId() << R_DIMMER << val;

    return QUrl(answer);
}

bool veraController::getStateOfSensor(int id)
{
    if (id < (int) sensors.size())
        return sensors[id].getState();

    return false;
}

bool veraController::getSensorTripped(int id)
{
    if (id < (int) sensors.size())
        return sensors[id].getTripped();

    return false;
}

bool veraController::getStateOfSiren(int id, bool lamp)
{
    if (id < (int) sirens.size())
    {
        if (lamp)
            return sirens[id].getStateLamp();
        else
            return sirens[id].getStateSiren();
    }
    return false;
}

void veraController::changeStateSiren(int id, bool siren, bool lamp)
{
    int level = 0; // 0=>OFF / 1->33=>light on / 67->100=>light+sound on
    if (siren)
        level += 67;
    if (lamp)
        level += 1;

    QNetworkRequest request(changeSirenValueUrl(id, level));
    netManager->get(request);

    sirens[id].setSiren(siren||lamp);
    sirens[id].setLamp(lamp);

    emit update(true, Vera::siren, id);
    setTimer(refreshTimer->isActive(), timerPeriod);
}

QUrl veraController::changeSirenValueUrl(const int num, const int val)
{
    if (num > (int) sirens.size())
        return QUrl();

    QString answer;
    QTextStream(&answer) << server << R_ACTION << sirens[num].getId() << R_DIMMER << val;

    return QUrl(answer);
}

void veraController::sslErrors(QNetworkReply *reply, QList<QSslError> errors)
{
    qDebug() << "SSL Error";
}

void veraController::refresh()
{
    QUrl u(server + R_DATA);
    QNetworkRequest request(u);
    netManager->get(request);

    QObject::connect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(updateNetwork(QNetworkReply*)));

}

void veraController::updateNetwork(QNetworkReply *reply)
{
    QObject::disconnect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(updateNetwork(QNetworkReply*)));

    // Parse JSON
    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    if (doc.isNull()) {
        emit update(false);
        reply->deleteLater();
        return;
    }

    int lightsIdx = 0, dimmersIdx = 0, sensorsIdx = 0, sirensIdx = 0, blindsIdx = 0;

    QJsonObject obj = doc.object();

    // Create device vectors
    QJsonArray devices = obj.value(J_DEVICES).toArray(QJsonArray()); // Empty array by default
    if (devices.size() == 0) {
        emit update(false);
        reply->deleteLater();
        return;
    }

    for (int i=0; i<devices.size(); i++)
    {
        QJsonObject curDevice = devices.at(i).toObject();
        QJsonArray curStates = curDevice.value(J_STATES).toArray();

        QString name = curDevice.value(J_NAME).toString();

        int cat = curDevice.value(J_CATEGORY).toInt(0);
        if (cat == 0)
            cat = curDevice.value(J_CATEGORY).toString().toInt();

        QJsonObject curState;
        switch (cat)
        {
            case CAT_BINLIGHT: // binary light

                // Name has changed -> not handled by this function
                if (name != QString::fromStdString( lights[lightsIdx].getName() )) {
                    emit (update(false));
                    return;
                }

                for (int j=0; j<curStates.size(); j++) {

                    curState = curStates.at(j).toObject();
                    if (curState.value(STA_VAR).toString() == VAR_STATUS) {

                        // If state has changed
                       if ((curState.value(STA_VAL).toString() == "1"
                                    && lights[lightsIdx].getState() == false)
                                || (curState.value(STA_VAL).toString() == "0"
                                    && lights[lightsIdx].getState() == true)) {
                           lights[lightsIdx].changeState();
                           emit (update(true, Vera::binLight, lightsIdx));
                       }
                    break; // for
                    }
                }
                lightsIdx++;
            break; // case

            case CAT_DIMMER: // dimmable light

                // Name has changed -> not handled by this function
                if (name != QString::fromStdString( dimmers[dimmersIdx].getName() )) {
                    emit (update(false));
                    return;
                }

                for (int j=0; j<curStates.size(); j++) {

                    curState = curStates.at(j).toObject();
                    if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                        // If state has changed
                       if (curState.value(STA_VAL).toString() !=
                               QString::number(dimmers[dimmersIdx].getLevel() ) ) {
                           dimmers[dimmersIdx].setLevel (curState.value(STA_VAL).toString().toInt());
                           emit (update(true, Vera::dimmer, dimmersIdx));
                       }
                    break; // for
                    }
                }
                dimmersIdx++;
            break; // case

            case CAT_SENSOR: // sensor

                // Name has changed -> not handled by this function
                if (name != QString::fromStdString( sensors[sensorsIdx].getName() )) {
                    emit (update(false));
                    return;
                }

                for (int j=0; j<curStates.size(); j++) {

                    curState = curStates.at(j).toObject();
                    if (curState.value(STA_VAR).toString() == VAR_SENSORTRIPPED) {
                        // If sensor is tripped
                        bool t = sensors[sensorsIdx].getTripped();
                        if ((curState.value(STA_VAL).toString() == "1"
                                     && t == false)
                                 || (curState.value(STA_VAL).toString() == "0"
                                     && t == true)) {
                            sensors[sensorsIdx].setTripped(!t);
                            emit sensorTripped(!t, sensorsIdx);
                        }
                    }
                    if (curState.value(STA_VAR).toString() == VAR_SENSORARMED) {
                        // If state has changed
                        if (curState.value(STA_VAL).toString() == "0")
                            int a = 0;
                       if ((curState.value(STA_VAL).toString() == "1"
                                    && sensors[sensorsIdx].getState() == false)
                                || (curState.value(STA_VAL).toString() == "0"
                                    && sensors[sensorsIdx].getState() == true)) {
                           sensors[sensorsIdx].changeState();
                           emit update(true, Vera::sensor, sensorsIdx);
                       }
                    }
                }
                sensorsIdx++;
            break; // case

            case CAT_SIRENS: // sirens

                // Name has changed -> not handled by this function
                if (name != QString::fromStdString( sirens[sirensIdx].getName() )) {
                    emit (update(false));
                    return;
                }

                for (int j=0; j<curStates.size(); j++) {

                    curState = curStates.at(j).toObject();
                    if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                        // If state has changed
                        int level = curState.value(STA_VAL).toString().toInt();
                        bool l (level > 0), s (level > 66);

                        if (l != sirens[sirensIdx].getStateLamp() || s != sirens[sirensIdx].getStateSiren()) {
                           sirens[sirensIdx].setSiren(s);
                           sirens[sirensIdx].setLamp(l);

                           emit (update(true, Vera::siren, sirensIdx));
                       }
                    break; // for
                    }
                }
                sirensIdx++;
            break; // case

        case CAT_BLIND: //blinds
            // Name has changed -> not handled by this function
            if (name != QString::fromStdString( blinds[blindsIdx].getName() )) {
                emit (update(false));
                return;
            }

            for (int j=0; j<curStates.size(); j++) {

                curState = curStates.at(j).toObject();
                if (curState.value(STA_VAR).toString() == VAR_DIMMERSTATUS) {

                    // If state has changed
                   if (curState.value(STA_VAL).toString() !=
                           QString::number(blinds[blindsIdx].getLevel() ) ) {
                       blinds[blindsIdx].setLevel (curState.value(STA_VAL).toString().toInt());
                       emit (update(true, Vera::blind, blindsIdx));
                   }
                break; // for
                }
            }
            blindsIdx++;
        break; // case
        }
    }

    emit update(true); // Used to signal that the Vera isi still answering, even the network did not change
    reply->deleteLater();
}

void veraController::setTimer(bool on, int msec)
{
    if (on) {
        refreshTimer->start(msec);
        timerPeriod = msec;
    }
    else {
        refreshTimer->stop();
    }
}

int veraController::getStateOfBlind(int id)
{
    if (id < (int) blinds.size())
        return blinds[id].getLevel();

    return 0;
}

QUrl veraController::changeBlindValueUrl(const int num, const int val)
{
    if (num > (int) blinds.size())
        return QUrl();

    QString answer;
    QTextStream(&answer) << server << R_ACTION << blinds[num].getId() << R_BLIND << val;

    return QUrl(answer);
}
/*
void veraController::switchLamp(QString str)
{
    for (int i = 0; i < lights.size(); i++)
    {
        if (str == getNameOf(Vera::binLight, i))
        {
            QNetworkRequest request(switchLampUrl(i));
            netManager->get(request);

            lights[i].changeState();
        }
    }
}
*/
void veraController::alert(int level)
{
    if (level == 1) /* ****** */
    {
        for (int i=0; i<(int) sirens.size(); i++)
            changeStateSiren(i, false, true);
        for (int i=0; i<(int) lights.size(); i++)
        {
            if (!getStateOfLamp(i))
                switchLamp(i);
        }
    } /* ******************** */
}

void veraController::changeValueBlind(int id, int val)
{
    QNetworkRequest request(changeBlindValueUrl(id, val));
    netManager->get(request);

    blinds[id].setLevel(val);

    emit update(true, Vera::blind, id);
    setTimer(refreshTimer->isActive(), timerPeriod);
}
