#ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QGraphicsView>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSignalMapper>
#include <QPainter>
#include <QBrush>

#include "modelo.h"
#include "miboton.h"

class Modelo;

class Principal : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Actuadores: Constructor que crea el layout de la ventana, asignándolo
     * a un puntero a QGraphicsView para poder mostrarlo.
     * @param parent
     */
    Principal(QWidget *parent = 0);
    ~Principal();

    /**
     * @brief showMaximized: Función que dibuja el layout de la pantalla y la muestra en pantalla completa.
     * @param modoClick: Modo activo para hacer click (Fijación o Guiño), que cambiará el texto de la etiqueta correspondiente.
     * @param activo: Modo activo de funcionamiento (Ojos, Táctil, Voz, Gestos), para establecer el botón correspondiente como pulsado.
     */
    void showMaximized();
    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void SetModel(Modelo &m);
    /**
     * @brief hide: Esconde la ventana.
     */
    void hide();
    /**
     * @brief DibujarBotones: Dibuja el botón recibido por parámetro en el layout de Favoritos.
     * @param bot
     */
    void DibujarBotones(miBoton * bot);
    /**
     * @brief EliminarBotones: Elimina el botón recibido por parámetro en el layout de Favoritos.
     * @param bot
     */
    void EliminarBotones(miBoton *bot);
    /**
     * @brief DibujarDimmer: Dibuja el layout del dimmer recibido por parámetro.
     * @param widget
     */
    void DibujarDimmer(QWidget *widget);
    void EliminarDimmer(QWidget *widget);
    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param str: hoja de estilos recibida.
     */
    void styleSheet(QString str);
    /**
     * @brief cambioModo: Función que cambia el texto de la etiqueta que especifica el modo de hacer click activo.
     * @param str
     */
    void cambioModo(QString str);

    void clickarBoton(QString str);

private:
    /**
     * @brief LayoutCategorias: Establece el layout vertical correspondiente a las categorías
     * (parte izquierda de la pantalla principal)
     */
    void LayoutCategorias();
    /**
     * @brief LayoutModos: Establece el layout horizontal de los modos disponibles.
     * @param modoClick: Modo activo de click para editar la etiqueta.
     * @param activo: Modo activo de funcionamiento para mantener el botón clickado.
     */
    void LayoutModos();
    /**
     * @brief LayoutFavoritos: Establece el grupo y el layout de los favoritos que se añaden.
     */
    void LayoutFavoritos();

    Modelo *miModelo;

    QGridLayout *layout;
    QGraphicsView *vista;

    //Modos de funcionamiento
    miBoton *ModoOjos;
    QLabel *etiquetaOjos;
    miBoton *ModoTactil;
    QLabel *etiquetaTactil;
    miBoton *ModoVoz;
    QLabel *etiquetaVoz;
    miBoton *ModoGestos;
    QLabel *etiquetaGestos;

    //Diferentes Ventanas
    miBoton *Configuracion;
    miBoton *Sensores;
    miBoton *Actuadores;
    miBoton *Imagenes;

    //Favoritos
    QGroupBox *Favs;
    QVBoxLayout *layoutfavoritos;
    QVector<miBoton *>Favoritos;


private slots:
    /**
     * @brief Los siguientes 4 slots (Configuracion, Sensores, Actuadores, Imagenes)
     * se llaman cuando el usuario pulsa en el botón correspondiente y dentro de ellos,
     * se avisa al modelo del cambio de ventana que tiene que hacer.
     */
    void configuracion();
    void sensores();
    void actuadores();
    void imagenes();

    /**
     * @brief cambiarmodo: Slot llamado por los 4 botones de modo de funcionamiento (Ojos, Tactil, Voz, Gestos)
     * desde el que se comprueba qué botón está pulsado y se avisa al modelo del cambio de modo que tiene que hacer.
     */
    void cambiarmodo();
};

#endif // PRINCIPAL_H
