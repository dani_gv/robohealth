#include "bbdd.h"

BBDD::BBDD()
{
    tiempoFijacion = 2000;
    tiempoCierre = 1500;
    tiempoGuino = 500;
}

void BBDD::EliminarUsuario(QString user)
{
    for (int i = 0; i < usuarios.size(); i++)
    {
        if (usuarios[i] == user)
            usuarios.erase(usuarios.begin()+i);
    }
}

void BBDD::setUsuarioActivo(QString user)
{
    usuarioActivo = user;
}

QString BBDD::getUsuarioActivo()
{
    if (usuarios.size() > 0)
        return usuarioActivo;
    return "ERROR";
}

void BBDD::setUsuarios(vector<QString> users)
{
    usuarios.clear();
    for (int i = 0; i < users.size(); i++)
        usuarios.push_back(users[i]);
}

vector<QString> BBDD::GetUsuarios()
{
    return usuarios;
}

QString BBDD::getModoActivo()
{
    return modoActivo;
}

void BBDD::setModoActivo(QString str)
{
    modoActivo = str;
}

QString BBDD::getModoClick()
{
    return modoClick;
}

void BBDD::setModoClick(QString str)
{
    modoClick = str;
}

void BBDD::setTiempoCierre(int t)
{
    tiempoCierre = t;
}

void BBDD::setTiempoGuino(int t)
{
    tiempoGuino = t;
}

void BBDD::setTiempoFijacion(int t)
{
    tiempoFijacion = t;
}

int BBDD::getTiempoCierre()
{
    return tiempoCierre;
}

int BBDD::getTiempoGuino()
{
    return tiempoGuino;
}

int BBDD::getTiempoFijacion()
{
    return tiempoFijacion;
}

void BBDD::AnyadirFavorito(QString fav)
{
    favoritos.push_back(fav);
}

void BBDD::EliminarFavorito(QString fav)
{
    for (int i = 0; i < favoritos.size(); i++)
        if (favoritos[i] == fav)
            favoritos.erase(favoritos.begin()+i);
}

QVector<QString> BBDD::GetFavoritos()
{
    return favoritos;
}

void BBDD::AnyadirDevice(QString dev)
{
    devices.push_back(dev);
}

QVector<QString> BBDD::GetDevices()
{
    return devices;
}

void BBDD::setPantallaActiva(QString str)
{
    pantallaActiva = str;
}

QString BBDD::getPantallaActiva()
{
    return pantallaActiva;
}
