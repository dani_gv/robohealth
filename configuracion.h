#ifndef CONFIGURACION_H
#define CONFIGURACION_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QGraphicsView>
#include <QListWidget>
#include <QGroupBox>
#include <QLabel>
#include <vector>
#include <QDebug>

#include "modelo.h"

using namespace std;

class Modelo;

class Configuracion : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Configuracion: Constructor que crea el layout de la ventana, asignándolo
     * a un puntero a QGraphicsView para poder mostrarlo.
     * @param parent
     */
    explicit Configuracion(QWidget *parent = 0);

    /**
     * @brief showMaximized: Muestra la ventana, a pantalla completa
     */
    void showMaximized();
    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void SetModel(Modelo &m);
    /**
     * @brief hide: Esconde la ventana.
     */
    void hide();
    /**
     * @brief SetUsuarios: Función que elimina todos los usuarios actuales de la lista
     * y la rellena con los usuarios disponibles. Se usa al principio del programa y cuando
     * hay alguna modificación en los usuarios disponibles, al crear o eliminar, por ejemplo.
     * @param users: Lista de Usuarios disponibles.
     */
    void SetUsuarios(vector<QString> users);
    /**
     * @brief SetUsuarioActivo: Establece el usuario actico para actualizar la etiqueta en la
     * que se especifica quien es el usuario activo en cada momento.
     * @param str
     */
    void SetUsuarioActivo(QString user);
    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param str: hoja de estilos recibida.
     */
    void styleSheet(QString str);

private:
    QGridLayout *layout;
    QGraphicsView *vista;

    Modelo *miModelo;


    QPushButton *Volver;
    //Favoritos
        //Listas
    QListWidget *listaDispositivos;
    QListWidget *listaFavoritos;

        //Botones
    QPushButton *AnadirFavorito;
    QPushButton *BorrarFavorito;


    //Tobii
        //Usuarios
    QPushButton *recalibrar;
    QPushButton *crear;
    QPushButton *eliminar;
    QPushButton *comprobar;
    QPushButton *cambiar;

    QLabel *UsuarioActual;
    QListWidget *ListaUsuarios;

        //Funcionamiento
    QPushButton *ModoGuino;
    QPushButton *ModoFijacion;
    QSlider *TiempoFijacion;
    QSlider *TiempoGuino;
    QSlider *TiempoCierre;
    QLabel *EtiquetaTiempoFijacion;
    QLabel *EtiquetaTiempoGuino;
    QLabel *EtiquetaModoActivo;
    QLabel *EtiquetaTiempoCierre;


    /**
     * @brief LayoutFavoritos: Método que realiza el diseño de toda la parte concerniente a los
     * favoritos de esta pantalla. Consta de 1 QGrupBox que envuelve a 2 listas de usuarios y 2 botones.
     * Los dos botones forman un layout vertical y, todo el conjunto, un layout horizontal.
     */
    void LayoutFavoritos();
    /**
     * @brief LayoutTobii: Método que realiza el diseño de la parte concerniente al EyeTracker.
     * Consta de 1 QGroupBox, la lista de usuarios y todos los botones de acciones que se pueden
     * realizar sobre los usuarios, además de las posibilidades de modificación de los tiempos establecidos.
     */
    void LayoutTobii();

private slots:
    /**
     * @brief volver: Slot del botón Volver para regresar a la pantalla principal.
     */
    void volver();

    //Favoritos
    /**
     * @brief anadirfav: Slot llamado cuando se pulsa a Añadir Favorito que comprueba que haya un dispositivo
     * seleccionado y, posteriormente, lo incluye en la lista de Favoritos. Llama al método AnyadirFavorito del
     * modelo para añadirlo a la BBDD.
     */
    void anadirfav();
    /**
     * @brief borrarfav: Slot llamado cuando se pulsa Eliminar Favorito que comprueba que haya algún favorito en la lista
     * y, además, que haya alguno seleccionado para poder borrarlos. Además, avisa al modelo de que se ha eliminado
     * un favorito para borrarlo de la BBDD.
     */
    void borrarfav();

    //Tobii
    /**
     * @brief Recalibrar: Slot llamado al pulsar el botón Recalibrar que avisa al modelo de la petición del usuario.
     */
    void Recalibrar();
    /**
     * @brief Crear: Slot llamado al pulsar el botón Crear que avisa al modelo de que el usuario quiere crear otro usuario nuevo.
     */
    void Crear();
    /**
     * @brief Eliminar: Slot llamado al pulsar el botón Eliminar que avisa al modelo de que se quiere eliminar un usuario.
     * Además, tiene que comprobar que haya algún usuario seleccionado de la lista antes de avisar al modelo.
     */
    void Eliminar();
    /**
     * @brief Comprobar: Slot llamado al pulsar el botón Comprobar para avisar al modelo de que se quiere comprobar la calibración del usuario actual.
     */
    void Comprobar();
    /**
     * @brief Cambiar: Slot llamado al pulsar el botón Cambiar que comprueba que haya algún usuario seleccionado de la lista
     * y avisa al modelo de que se quiere realizar el cambio a ese usuario.
     */
    void Cambiar();
    /**
     * @brief modofijacion: Método llamado al pulsar el botón Fijación que cambia el texto que muestra la etiqueta sobre el modo activo.
     * y, además, avisa al modelo para que cambie el modo del click en la BBDD.
     */
    void modofijacion();
    /**
     * @brief modoguino: Método que realiza la misma tarea que ModoFijacion pero activando el Guiño.
     */
    void modoguino();
    /**
     * @brief TiempoFijacionCambiado: Slot llamado cada vez que se realiza un cambio en el tiempo mínimo para que una fijación se considere click.
     */
    void TiempoFijacionCambiado(int);
    /**
     * @brief TiempoGuinoCambiado: Slot que realiza la misma tarea que TiempoFijacionCambiado pero con respecto al guiño.
     */
    void TiempoGuinoCambiado(int);
    /**
     * @brief TiempoCierreCambiado: Slot que realiza la misma tarea que TiempoFijacionCambiado pero con el tiempo de cierre de ojos.
     */
    void TiempoCierreCambiado(int);

};

#endif // CONFIGURACION_H
