#ifndef ACTUADORES_H
#define ACTUADORES_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QGraphicsView>
#include <QVector>
#include <QString>

#include "miboton.h"
#include "modelo.h"

using namespace std;

class Modelo;

class Actuadores : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Actuadores: Constructor que crea el layout de la ventana, asignándolo
     * a un puntero a QGraphicsView para poder mostrarlo.
     * @param parent
     */
    explicit Actuadores(QWidget *parent = 0);
    ~Actuadores();

    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void setModel(Modelo &m);
    /**
     * @brief showMaximized: Muestra la ventana, a pantalla completa
     */
    void showMaximized();
    /**
     * @brief hide: Esconde la ventana.
     */
    void hide();

    /**
     * @brief MostrarDispositivo: Coloca el botón pasado por parámetro en el layout
     * @param b: Botón recibido desde el modelo
     */
    void MostrarDispositivo(miBoton *b);
    /**
     * @brief LimpiarDispositivo: Elimina del layout el botón pasado por parámetro.
     * @param b: Botón recibido desde el modelo.
     */
    void LimpiarDispositivo(miBoton *b);
    /**
     * @brief MostrarDimmer: Muestra el dimmer recibo desde el modelo.
     * @param widget: Contiene el layout del dimmer, puesto que incluye 2 botones y una etiqueta.
     */
    void MostrarDimmer(QWidget *widget);
    /**
     * @brief EliminarDimmer: Elimina del layout el dimmer recibido por parámetro.
     * @param widget contiene el layout del dimmer.
     */
    void EliminarDimmer(QWidget *widget);
    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param str: hoja de estilos recibida.
     */
    void styleSheet(QString str);

private:
    Modelo *miModelo;

    QGraphicsView *vista;
    QGridLayout *layout;

    const int maximoBotonesFila = 3;
    int fila;
    int columna;

    miBoton *volver;


signals:

public slots:
    /**
     * @brief Volver: Slot llamado por el botón volver que regresa a la pantalla principal.
     */
    void Volver();
};

#endif // ACTUADORES_H
