#ifndef CUADROTEXTO_H
#define CUADROTEXTO_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QGraphicsView>

#include "modelo.h"
#include "miboton.h"

using namespace std;


class Modelo;

class CuadroTexto : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief CuadroTexto es el constructor de la clase, que crea todos los objetos disponibles en esta clase.
     * @param parent
     */
    explicit CuadroTexto(QWidget *parent = 0);
    ~CuadroTexto();
    /**
     * @brief setTexto establece el texto a poner en el cuadro de texto
     * @param str
     */
    void setTexto(QString str);
    /**
     * @brief show Muestra el cuadro de texto.
     */
    void show();

    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void setModel(Modelo &m);

    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param style: hoja de estilos recibida.
     */
    void styleSheet(QString style);
private:
    miBoton *aceptarButton;
    QLabel *informacion;

    QVBoxLayout *layout;
    QGraphicsView *vista;

    Modelo *miModelo;
signals:

public slots:
    /**
     * @brief Aceptar slot del botón aceptar que esconde la vista para que se pueda seguir con el programa.
     */
    void Aceptar();
};

#endif // CUADROTEXTO_H
