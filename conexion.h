#ifndef CONEXION_H
#define CONEXION_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QGridLayout>
#include <QGraphicsView>

#include "modelo.h"

class Modelo;

using namespace std;

class Conexion : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Conexion: Constructor que crea el layout de la ventana, asignándolo
     * a un puntero a QGraphicsView para poder mostrarlo.
     * @param parent
     */
    explicit Conexion(QWidget *parent = 0);

    /**
     * @brief setModel: Hace la unión entre el modelo y la vista
     * @param m: Objeto a modelo
     */
    void SetModel(Modelo &m);
    /**
     * @brief show: Muestra la ventana, con tamaño mínimo establecido en el constructor
     */
    void show();
    /**
     * @brief hide: Esconde la ventana.
     */
    void hide();
    /**
     * @brief styleSheet: Actualiza el estilo de la ventana.
     * @param str: hoja de estilos recibida.
     */
    void styleSheet(QString str);

private:
    QLineEdit *ip;
    QPushButton *Conectar;

    QGridLayout *layout;
    QGraphicsView *vista;

    Modelo *miModelo;
signals:

public slots:

private slots:
    /**
     * @brief urlEdited: slot unido al botón conectar que comprueba que la ip introducida sea válida
     * y, en ese caso, llama al método connection del objeto miModelo.
     */
    void urlEdited();
};

#endif // CONEXION_H
