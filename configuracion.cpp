#include "configuracion.h"

Configuracion::Configuracion(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout();
    vista = new QGraphicsView();

    Volver = new QPushButton("Volver");
    connect(Volver, SIGNAL(clicked(bool)), this, SLOT(volver()));
    layout->addWidget(Volver, 0, 3, 1, 1);

    LayoutFavoritos();
    LayoutTobii();

    layout->setSpacing(20);
    vista->setLayout(layout);
}

void Configuracion::showMaximized()
{
    for (int i = miModelo->GetDevices().size()-1; i >= 0; i--)
        listaDispositivos->takeItem(i);    
    for (unsigned int i = 0; i < miModelo->GetDevices().size(); i++)
        listaDispositivos->addItem(miModelo->GetDevices()[i]);
    vista->showMaximized();
}

void Configuracion::SetModel(Modelo &m)
{
    miModelo = &m;
}

void Configuracion::hide()
{
    vista->hide();
}

void Configuracion::SetUsuarios(vector<QString> users)
{
    for (int i = ListaUsuarios->count(); i >= 0; i--)
        ListaUsuarios->takeItem(i);

    for (int i = 0; i < users.size(); i++)
        ListaUsuarios->addItem(users[i]);
}

void Configuracion::SetUsuarioActivo(QString user)
{
    QString aux;
    aux = "Usuario Actual: " + user;
    UsuarioActual->setText(aux);
}

void Configuracion::styleSheet(QString str)
{
    vista->setStyleSheet(str);
}

void Configuracion::LayoutFavoritos()
{
    QGroupBox *fav = new QGroupBox("Favoritos");
    QHBoxLayout *favsLayout = new QHBoxLayout();
    QVBoxLayout *favsDisp = new QVBoxLayout();

    listaDispositivos = new QListWidget();
    listaDispositivos->setMaximumWidth(300);
    listaDispositivos->setStyleSheet("background-color: rgb(23,44,86)");
    listaDispositivos->setMaximumHeight(250);

    listaFavoritos = new QListWidget();
    listaFavoritos->setMaximumWidth(300);
    listaFavoritos->setStyleSheet("background-color: rgb(23,44,86)");
    listaFavoritos->setMaximumHeight(250);

    AnadirFavorito = new QPushButton("Añadir Favorito");
    connect(AnadirFavorito, SIGNAL(clicked()), this, SLOT(anadirfav()));
    BorrarFavorito = new QPushButton("Borrar Favorito");
    connect(BorrarFavorito, SIGNAL(clicked(bool)), this, SLOT(borrarfav()));

    favsDisp->addWidget(AnadirFavorito);
    favsDisp->addWidget(BorrarFavorito);

    favsLayout->addWidget(listaDispositivos);
    favsLayout->addLayout(favsDisp);
    favsLayout->addWidget(listaFavoritos);

    fav->setLayout(favsLayout);
    fav->setMaximumHeight(275);

    layout->addWidget(fav, 1, 1, 1, 3);
}

void Configuracion::LayoutTobii()
{
    QGroupBox *groupTobii = new QGroupBox("Configuracion Tobii");
//    groupTobii->setStyleSheet("background-color: rgb(8,21,35)");

    QVBoxLayout *layoutTobii = new QVBoxLayout();
    QHBoxLayout *layoutUsers = new QHBoxLayout();
    layoutUsers->setSpacing(20);

    UsuarioActual = new QLabel("Usuario Actual: ");

    recalibrar = new QPushButton("Recalibrar Dispositivo");
    connect(recalibrar, SIGNAL(clicked(bool)), this, SLOT(Recalibrar()));
    comprobar = new QPushButton("Comprobar Calibracion Dispositivo");
    connect(comprobar, SIGNAL(clicked(bool)), this, SLOT(Comprobar()));

    ListaUsuarios = new QListWidget();
    ListaUsuarios->setStyleSheet("background-color: rgb(23,44,86)");

    QGroupBox *title = new QGroupBox("Usuarios Disponibles");
    QGridLayout *l = new QGridLayout();
    l->addWidget(ListaUsuarios);
    title->setLayout(l);
    title->setMaximumWidth(310);


    crear = new QPushButton("Crear");
    eliminar = new QPushButton("Eliminar");
    cambiar = new QPushButton("Cambiar");
    connect(crear, SIGNAL(clicked(bool)), this, SLOT(Crear()));
    connect(eliminar, SIGNAL(clicked(bool)), this, SLOT(Eliminar()));
    connect(cambiar, SIGNAL(clicked(bool)), this, SLOT(Cambiar()));

    layoutUsers->addWidget(title);
    QVBoxLayout *buttons = new QVBoxLayout();
    buttons->setContentsMargins(100,0,100,0);
    buttons->setSpacing(20);
    buttons->addWidget(recalibrar);
    buttons->addWidget(comprobar);
    buttons->addWidget(cambiar);
    buttons->addWidget(eliminar);
    buttons->addWidget(crear);

    EtiquetaModoActivo = new QLabel("Modo Activo: Fijacion");
    buttons->addWidget(EtiquetaModoActivo);

    //Fijacion
    QHBoxLayout *fixationLayout = new QHBoxLayout();
    EtiquetaTiempoFijacion = new QLabel("Tiempo Actual Fijacion: 2000s");
    ModoFijacion = new QPushButton("Modo Fijacion");
    connect(ModoFijacion, SIGNAL(clicked()), this, SLOT(modofijacion()));
    ModoFijacion->setCheckable(true);
    ModoFijacion->setMinimumWidth(350);
    ModoFijacion->setAutoExclusive(true);
    ModoFijacion->setChecked(true);

    TiempoFijacion = new QSlider(Qt::Horizontal);
    TiempoFijacion->setRange(500, 4000);
    TiempoFijacion->setSliderPosition(2000);
    connect(TiempoFijacion, SIGNAL(valueChanged(int)), this, SLOT(TiempoFijacionCambiado(int)));

    fixationLayout->addWidget(ModoFijacion);
    fixationLayout->addWidget(TiempoFijacion);
    fixationLayout->addWidget(EtiquetaTiempoFijacion);

    buttons->addLayout(fixationLayout);

    //Guiño
    QHBoxLayout *winkLayout = new QHBoxLayout();
    EtiquetaTiempoGuino = new QLabel("Tiempo Actual Guiño: 500s");
    ModoGuino = new QPushButton("Modo Guino");
    connect(ModoGuino, SIGNAL(clicked()), this, SLOT(modoguino()));
    ModoGuino->setCheckable(true);
    ModoGuino->setMinimumWidth(350);
    ModoGuino->setAutoExclusive(true);
    ModoGuino->setChecked(false);
    TiempoGuino = new QSlider(Qt::Horizontal);
    TiempoGuino->setRange(300, 2000);
    TiempoGuino->setSliderPosition(500);
    connect(TiempoGuino, SIGNAL(valueChanged(int)), this, SLOT(TiempoGuinoCambiado(int)));

    winkLayout->addWidget(ModoGuino);
    winkLayout->addWidget(TiempoGuino);
    winkLayout->addWidget(EtiquetaTiempoGuino);

    buttons->addLayout(winkLayout);

    //Cierre
    QHBoxLayout *cierreLayout = new QHBoxLayout();
    QLabel *cierre = new QLabel("Cambio Modo por Cierre");
    EtiquetaTiempoCierre = new QLabel("Tiempo Actual Cambio: 1500s");
    TiempoCierre = new QSlider(Qt::Horizontal);
    TiempoCierre->setRange(500, 4000);
    TiempoCierre->setSliderPosition(1500);
    connect(TiempoCierre, SIGNAL(valueChanged(int)), this, SLOT(TiempoCierreCambiado(int)));
    cierreLayout->addWidget(cierre);
    cierreLayout->addWidget(TiempoCierre);
    cierreLayout->addWidget(EtiquetaTiempoCierre);
    buttons->addLayout(cierreLayout);


    layoutUsers->addLayout(buttons);
    layoutTobii->addWidget(UsuarioActual);

    //layoutTobii->addWidget(recalibrar);
   // layoutTobii->addWidget(comprobar);
    layoutTobii->addLayout(layoutUsers);

    groupTobii->setLayout(layoutTobii);
    layout->addWidget(groupTobii, 2, 1, 3, 3);
}

void Configuracion::volver()
{
    miModelo->CambioVentana("Principal");
}

void Configuracion::anadirfav()
{
    if (listaDispositivos->currentRow() == -1)
    {
        miModelo->crearCuadro("Debe Seleccionar un dispositivo de la lista");
        return;
    }
    else
    {
        if(miModelo->GetFavoritos().size() >= 4)
        {
            miModelo->crearCuadro("Ha superado el límite de favoritos");
            return;
        }
        QString device(listaDispositivos->item(listaDispositivos->currentRow())->text());
        for (unsigned int i = 0; i < (miModelo->GetFavoritos().size()); i++)
        {
            if (device == listaFavoritos->item(i)->text())
            {
                miModelo->crearCuadro("Este dispositivo ya se encuentra en la lista de favoritos");
                return;
            }
        }
        miModelo->AnyadirFavorito(device);
        listaFavoritos->addItem(device);
        listaDispositivos->item(listaDispositivos->currentRow())->setSelected(false);
    }
}

void Configuracion::borrarfav()
{
    if (miModelo->GetFavoritos().size() == 0)
    {
        miModelo->crearCuadro("No hay ningún dispositivo en la lista");
        return;
    }

    if(listaFavoritos->currentRow() == -1)
    {
        miModelo->crearCuadro("Debe seleccionar un dispositivo de la lista");
        return;
    }
    else
    {
        miModelo->EliminarFavorito(listaFavoritos->item(listaFavoritos->currentRow())->text());
        listaFavoritos->takeItem(listaFavoritos->currentRow());
    }
}

void Configuracion::Recalibrar()
{
    miModelo->RecalibrarUsuario();
}

void Configuracion::Crear()
{
    miModelo->CrearUsuario();
}

void Configuracion::Eliminar()
{
    if (ListaUsuarios->currentRow() == -1)
    {
        miModelo->crearCuadro("Debe seleccionar un usuario de la lista para poder eliminarlo");
        return;
    }
    else
    {
        QString usuario = ListaUsuarios->item(ListaUsuarios->currentRow())->text();
        miModelo->EliminarUsuario(usuario);
    }
}

void Configuracion::Comprobar()
{
    miModelo->ComprobarCalibracion();
}

void Configuracion::Cambiar()
{
    if (ListaUsuarios->currentRow() == -1)
    {
        miModelo->crearCuadro("Debe seleccionar un usuario de la lista de usuarios al que cambiar");
        return;
    }
    else
    {
        QString usuario = ListaUsuarios->item(ListaUsuarios->currentRow())->text();
        miModelo->CambiarUsuario(usuario);
    }
}

void Configuracion::modofijacion()
{
    EtiquetaModoActivo->setText("Modo Activo: Fijacion");
    miModelo->setModoClick("Fijacion");
}

void Configuracion::modoguino()
{
    EtiquetaModoActivo->setText("Modo Activo: Guiño");
    miModelo->setModoClick("Guino");
}

void Configuracion::TiempoFijacionCambiado(int time)
{
    QString str("Tiempo Actual Fijacion: ");
    str += QString::number(time);
    EtiquetaTiempoFijacion->setText(str);

    miModelo->TiempoFijacion(time);
}

void Configuracion::TiempoGuinoCambiado(int time)
{
    QString str("Tiempo Actual Guiño: ");
    str += QString::number(time);
    EtiquetaTiempoGuino->setText(str);

    miModelo->TiempoGuino(time);
}

void Configuracion::TiempoCierreCambiado(int time)
{
    QString str("Tiempo Actual Cierre: ");
    str += QString::number(time);
    EtiquetaTiempoCierre->setText(str);

    miModelo->TiempoCierre(time);
}
