#include "eyex.h"


//Variables globales
    //ID of the global interactor that provides our data stream; must be unique within the app
static TX_CONSTSTRING InteractorId = "Rainbow Dash";
static TX_HANDLE g_hGlobalInteractorSnapshot = TX_EMPTY_HANDLE;

//funciones necesarias para CALLBACK
void TX_CALLCONVENTION miSnapshot(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
    EyeX *aux = static_cast<EyeX *>(param);
    aux->OnSnapshotCommitted(hAsyncData, param);
}
void TX_CALLCONVENTION miHandle(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
    EyeX *aux = static_cast<EyeX *>(param);
    aux->HandleEvent(hAsyncData, param);
}
void TX_CALLCONVENTION miEngine(TX_CONNECTIONSTATE hAsyncData, TX_USERPARAM param)
{
    EyeX *aux = static_cast<EyeX *>(param);
    aux->OnEngineConnectionStateChanged(hAsyncData, param);
}
void TX_CALLCONVENTION miEngineStateChanged(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
    EyeX *aux = static_cast<EyeX *>(param);
    aux->OnEngineStateChanged(hAsyncData, param);
}

EyeX::EyeX(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<vector<QString>>("vector<QString>");

    //Inicializacion Variables
//    OjosCerrados = false;
    OjosAbiertos.setHMS(0,0,0);
    OjosInteraccion.setHMS(0,0,0);
    Habilitacion = false;
    TiempoGuino = 500;
    TiempoCierre = 1500;
    //Fin Inicializacion!

    if (TX_RESULT_OK != txInitializeEyeX(TX_EYEXCOMPONENTOVERRIDEFLAG_NONE, NULL, NULL, NULL, NULL))
        return;

    //Inicializacion variables EyeX
    hContext = TX_EMPTY_HANDLE;
    TX_TICKET hConnectionStateChangedTicket = TX_INVALID_TICKET;
    TX_TICKET hEventHandlerTicket = TX_INVALID_TICKET;
    TX_TICKET hCurrentProfileNameChangedTicket = TX_INVALID_TICKET;
    TX_TICKET hEyeTrackingProfilesChangedTicket = TX_INVALID_TICKET;

    // initialize and enable the context that is our link to the EyeX Engine.
    success = txInitializeEyeX(TX_EYEXCOMPONENTOVERRIDEFLAG_NONE, NULL, NULL, NULL, NULL) == TX_RESULT_OK;
    success &= txCreateContext(&hContext, TX_FALSE) == TX_RESULT_OK;
    success &= InitializeGlobalInteractorSnapshot(hContext);
    success &= txRegisterConnectionStateChangedHandler(hContext, &hConnectionStateChangedTicket, miEngine, this) == TX_RESULT_OK;
    success &= txRegisterStateChangedHandler(hContext, &hCurrentProfileNameChangedTicket, TX_STATEPATH_EYETRACKINGCURRENTPROFILENAME, miEngineStateChanged, this) == TX_RESULT_OK;
    success &= txRegisterStateChangedHandler(hContext, &hEyeTrackingProfilesChangedTicket, TX_STATEPATH_EYETRACKINGPROFILES, miEngineStateChanged, this) == TX_RESULT_OK;
    success &= txRegisterEventHandler(hContext, &hEventHandlerTicket, miHandle, this) == TX_RESULT_OK;
    success &= txEnableConnection(hContext) == TX_RESULT_OK;
}

EyeX::~EyeX()
{
    txDisableConnection(hContext);
    txReleaseObject(&g_hGlobalInteractorSnapshot);
    success = txShutdownContext(hContext, TX_CLEANUPTIMEOUT_DEFAULT, TX_FALSE) == TX_RESULT_OK;
    success &= txReleaseContext(&hContext) == TX_RESULT_OK;
    success &= txUninitializeEyeX() == TX_RESULT_OK;
}

void EyeX::OnSnapshotCommitted(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
    // check the result code using an assertion.
    // this will catch validation errors and runtime errors in debug builds. in release builds it won't do anything.

    TX_RESULT result = TX_RESULT_UNKNOWN;
    txGetAsyncDataResultCode(hAsyncData, &result);
    //assert(result == TX_RESULT_OK || result == TX_RESULT_CANCELLED);
}

void EyeX::HandleEvent(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam)
{
    TX_HANDLE hEvent = TX_EMPTY_HANDLE;
    TX_HANDLE hBehavior1 = TX_EMPTY_HANDLE;
    TX_HANDLE hBehavior2 = TX_EMPTY_HANDLE;

    txGetAsyncDataContent(hAsyncData, &hEvent);

    //1
    if (txGetEventBehavior(hEvent, &hBehavior1, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK) {
        ComprobacionClick(hBehavior1);
        txReleaseObject(&hBehavior1);
    }

    //2
    if (txGetEventBehavior(hEvent, &hBehavior2, TX_BEHAVIORTYPE_FIXATIONDATA) == TX_RESULT_OK) {
        Position(hBehavior2);
        txReleaseObject(&hBehavior2);
    }

    txReleaseObject(&hEvent);
}

void EyeX::OnEngineConnectionStateChanged(TX_CONNECTIONSTATE connectionState, TX_USERPARAM userParam)
{
    if(connectionState == TX_CONNECTIONSTATE_CONNECTED)
    {
        bool success;
       // emit Connection_Information("The connection state is now CONNECTED (We are connected to the EyeX Engine)", false);

        // commit the snapshot with the global interactor as soon as the connection to the engine is established.
        // (it cannot be done earlier because committing means "send to the engine".)
        success = txCommitSnapshotAsync(g_hGlobalInteractorSnapshot, miSnapshot, this) == TX_RESULT_OK;
        if (!success)
            //emit Connection_Information("Failed to initialize the data stream", false);
            int a = 0;

        else
           // emit Connection_Information("Waiting for eye position data to start streaming...", false);
            int a = 0;


        txGetStateAsync(hContext, TX_STATEPATH_ENGINEINFOVERSION, miEngineStateChanged, this);
        txGetStateAsync(hContext, TX_STATEPATH_EYETRACKINGCURRENTPROFILENAME, miEngineStateChanged, this);
        txGetStateAsync(hContext, TX_STATEPATH_EYETRACKINGPROFILES, miEngineStateChanged, this);
    }
}

bool EyeX::InitializeGlobalInteractorSnapshot(TX_CONTEXTHANDLE hContext)
{
    TX_HANDLE hInteractor = TX_EMPTY_HANDLE;

    //1
    TX_HANDLE hBehaviorWithoutParameters = TX_EMPTY_HANDLE;
    //2
    TX_FIXATIONDATAPARAMS params = { TX_FIXATIONDATAMODE_SENSITIVE };

    //General
    bool success1;

    success1 = txCreateGlobalInteractorSnapshot(hContext, InteractorId, &g_hGlobalInteractorSnapshot, &hInteractor) == TX_RESULT_OK;
    //1
    success1 &= txCreateInteractorBehavior(hInteractor, &hBehaviorWithoutParameters, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK;
    //2
    success1 &= txCreateFixationDataBehavior(hInteractor, &params) == TX_RESULT_OK;

    //General
    txReleaseObject(&hInteractor);

    return success1;

}

void EyeX::OnEngineStateChanged(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam)
{
    TX_HANDLE hStateBag = TX_EMPTY_HANDLE;
    TX_RESULT result = TX_RESULT_UNKNOWN;

    if(txGetAsyncDataResultCode(hAsyncData, &result) == TX_RESULT_OK && txGetAsyncDataContent(hAsyncData, &hStateBag) == TX_RESULT_OK)
    {
        OnStateReceived(hStateBag);
        txReleaseObject(&hStateBag);
    }
}

void EyeX::CambiarUsuario(QString name)
{
    char *aux = (char *)malloc(name.size());
    QByteArray b = name.toLatin1();
    strcpy(aux,b.data());

    txSetCurrentProfile(hContext, aux, NULL, NULL);
}

void EyeX::EliminarUsuario(QString name)
{
    char *aux = (char *)malloc(name.size());
    QByteArray b = name.toLatin1();
    strcpy(aux, b.data());
    txDeleteProfile(hContext, aux, NULL, NULL);
}

void EyeX::CrearUsuario()
{
    txLaunchConfigurationTool(hContext, TX_CONFIGURATIONTOOL_CREATENEWPROFILE, NULL, NULL);
}

void EyeX::Recalibrar()
{
    txLaunchConfigurationTool(hContext, TX_CONFIGURATIONTOOL_RECALIBRATE, NULL, NULL);
}

void EyeX::ComprobarCalibracion()
{
    txLaunchConfigurationTool(hContext, TX_CONFIGURATIONTOOL_TESTEYETRACKING, NULL, NULL);
}

void EyeX::IniciarPrograma()
{
    Habilitacion = true;
}

void EyeX::CambiarTiempoGuino(int t)
{
    TiempoGuino = t;
}

void EyeX::CambiarTiempoCierre(int t)
{
    TiempoCierre = t;
}

void EyeX::OnStateReceived(TX_HANDLE hStateBag)
{
    TX_BOOL success2;
    TX_SIZE stringSize = 0;
    TX_STRING engineVersion;
    TX_STRING profileName;
    vector<string>profiles;

    success2 = (txGetStateValueAsString(hStateBag, TX_STATEPATH_ENGINEINFOVERSION, NULL, &stringSize) == TX_RESULT_OK);
    if(success2)
    {
        engineVersion = (TX_STRING)malloc(stringSize*sizeof(char));
        txGetStateValueAsString(hStateBag, TX_STATEPATH_ENGINEINFOVERSION, engineVersion, &stringSize);
        free(engineVersion);
    }


    success2 = (txGetStateValueAsString(hStateBag, TX_STATEPATH_EYETRACKINGCURRENTPROFILENAME, NULL, &stringSize) == TX_RESULT_OK);
    if(success2)
    {
        profileName = (TX_STRING)malloc(stringSize*sizeof(char));
        if(txGetStateValueAsString(hStateBag, TX_STATEPATH_EYETRACKINGCURRENTPROFILENAME, profileName, &stringSize) == TX_RESULT_OK)
        {

          UsuarioActual = profileName;
          emit SenyalUsuarioActual(UsuarioActual);

        }
        free(profileName);
    }


    success2 = GetStateValueAsArrayOfStrings(hStateBag, TX_STATEPATH_EYETRACKINGPROFILES, &profiles);
    if (success2)
    {
        Usuarios = profiles;
        vector<QString>aux;
        for (unsigned int i = 0; i < Usuarios.size(); i++)
        {
            aux.push_back(Usuarios[i].c_str());
        }
        emit ListaUsuarios(aux);
    }
}

TX_BOOL EyeX::GetStateValueAsArrayOfStrings(TX_CONSTHANDLE hStateBag, TX_CONSTSTRING valuePath, std::vector<string> *arrayOfStrings)
{
    TX_SIZE stringSize = 0;
    TX_BOOL success;

    success = (txGetStateValueAsString(hStateBag, valuePath, nullptr, &stringSize)) == TX_RESULT_OK;
    if (!success)
    {
        return TX_FALSE;
    }

    TX_STRING stringValue = new TX_CHAR[stringSize];
    success = (txGetStateValueAsString(hStateBag, valuePath, stringValue, &stringSize) == TX_RESULT_OK);
    if (!success)
    {
        delete[] stringValue;
        return TX_FALSE;
    }

    TX_STRING stringPtr = stringValue;
    while (*stringPtr != '\0')
    {
        TX_STRING startPoint = stringPtr;
        while (*stringPtr != '\0')
        {
            stringPtr++;
        }

        std::string str = std::string(startPoint, stringPtr - startPoint);
        arrayOfStrings->push_back(str);
        stringPtr++;
    }

    return TX_TRUE;
}

void EyeX::ComprobacionClick(TX_HANDLE hEyePositionDataBehavior)
{
    COORD position = {0,8};
    TX_EYEPOSITIONDATAEVENTPARAMS eventParams;

    if (txGetEyePositionDataEventParams(hEyePositionDataBehavior, &eventParams) == TX_RESULT_OK)
    {
        SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);

        ComprobacionOjos(eventParams.RightEyeX, eventParams.RightEyeY, eventParams.RightEyeZ, eventParams.LeftEyeX, eventParams.LeftEyeY, eventParams.LeftEyeZ);

        if (Habilitacion)
        {
            if (EstadoActual == AmbosOjosAbiertos)
            {
                OjosAbiertos = QTime::currentTime();
//                OjosCerrados = false;
                emit ojosAbiertos();
            }
            else
            {
                if (EstadoActual == AmbosOjosCerrados)
                {
                    OjosInteraccion = QTime::currentTime();
                    if (ComprobacionTiempos(OjosInteraccion, OjosAbiertos, TiempoCierre))
//                        if (OjosCerrados == false)
//                        {
//                            OjosCerrados = true;
                            emit cierreOjos();
//                        }
                }
                else
                {
//                    OjosCerrados = false;
                    OjosInteraccion = QTime::currentTime();
                    if (ComprobacionTiempos(OjosInteraccion, OjosAbiertos, TiempoGuino))
                    {
                        if (EstadoActual == OjoDerechoCerrado)
                            emit GuinoDerecho();
                        else if (EstadoActual == OjoIzquierdoCerrado)
                            emit GuinoIzquierdo();
                    }
                }
            }
        }
    }
}

bool EyeX::ComprobacionTiempos(QTime t1, QTime t2, int tiempoLimite)
{
    float tiempo1 = (t1.hour()*3600 + t1.minute()*60 + t1.second())*1000 + t1.msec();
    float tiempo2 = (t2.hour()*3600 + t2.minute()*60 + t2.second())*1000 + t2.msec();

    if (tiempo1 - tiempo2 >= tiempoLimite)
        return true;
    return false;
}

void EyeX::ComprobacionOjos(double RightEyeX, double RightEyeY, double RightEyeZ, double LeftEyeX, double LeftEyeY, double LeftEyeZ)
{
    const double zero = (double) 0.0f;

    if ((RightEyeX == zero) && (RightEyeY == zero) && (RightEyeZ == zero))
    {
        if ((LeftEyeX == zero) && (LeftEyeY == zero) && (LeftEyeZ == zero))
            EstadoActual = AmbosOjosCerrados;
        else
            EstadoActual = OjoDerechoCerrado;
    }
    else
    {
        if ((LeftEyeX == zero) && (LeftEyeY == zero) && (LeftEyeZ == zero))
            EstadoActual = OjoIzquierdoCerrado;
        else
            EstadoActual = AmbosOjosAbiertos;
    }
    return;
}

void EyeX::Position(TX_HANDLE hFixationDataBehavior)
{
    TX_FIXATIONDATAEVENTPARAMS eventParams;

    if (txGetFixationDataEventParams(hFixationDataBehavior, &eventParams) == TX_RESULT_OK)
        Filtro((float)eventParams.X, (float)eventParams.Y);
}

void EyeX::Filtro(float x_i, float y_i)
{
    x.push_back(x_i);
    y.push_back(y_i);

    if (x.size() > TamVentana && y.size() > TamVentana)
    {
        float x_salida = 0;
        float y_salida = 0;

        for (unsigned int i = x.size()-TamVentana-1; i < x.size(); i++)
        {
            x_salida += x[i];
            y_salida += y[i];
        }
        x_salida = x_salida/TamVentana;
        y_salida = y_salida/TamVentana;

        emit Posicionar(x_salida, y_salida);
    }
}
